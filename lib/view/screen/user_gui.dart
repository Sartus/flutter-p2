//https://medium.com/@afegbua/flutter-thursday-13-building-a-user-registration-and-login-process-with-provider-and-external-api-1bb87811fd1d
//https://bendyworks.com/blog/a-month-of-flutter-user-registration-form
//https://kodestat.gitbook.io/flutter/22-flutter-checkbox
//https://pusher.com/tutorials/flutter-listviews

import 'package:flutter/material.dart';
import 'package:pet/model/us_user_model.dart';
import 'package:pet/view/master_view.dart';
import 'package:pet/view/screen/user_address_gui.dart';

import 'package:pet/generated/l10n.dart';

class UserGui extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _State();
}

class _State extends State<UserGui> {
  MasterService _masterService = MasterService.instance;
  final nameController= TextEditingController();
  final pass1Controller= TextEditingController();
  final pass2Controller= TextEditingController();
  bool bEhOwner = true; //Owner of one or more Pets
  bool bEhWalker = false; //This is who walke with a Pet.
  String sErrorMessage="";
  //bool bIsUserForm;
  //bool bIsLoading;

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    nameController.dispose();
    pass1Controller.dispose();
    pass2Controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
    
      appBar: _appBar(),

      body: Stack( 
            children: <Widget> [
            _showForm(),
            //_showCircularProgress(),
            ],
      ),
      
      bottomNavigationBar: _showBottomNavigationBar(),

    );
  }


PreferredSizeWidget _appBar() {
  return AppBar(
    //leading: Icon(Icons.menu),
    leading: new IconButton( 
      icon: new Icon(
        Icons.menu,
        //color: Colors.green[500],
      ),
      onPressed: () {},
    ),    
    title: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget> [
        Icon(
          Icons.pets,
          //color: Colors.green[500],
        ),
        Padding(
          padding: const EdgeInsets.all(10.0),
        ),
        Text (
          "Pet Walking",
        ),
        Padding(
          padding: const EdgeInsets.all(30.0),
        ),
      ],
    ),
  );
}

Widget _showForm() {

  return Container(
    padding: EdgeInsets.all(10.0),
    child: new ListView(
      shrinkWrap: true,
      children: <Widget>[
        //_showLogo(),
//        oShowTitle,
        _showTitle(),
        _showNameInput(),
        _showPass1Input(),
        _showPass2Input(),
        _showCheckBoxInput(),
        _showButtons(),
        //_showErrorMessage(),
      ],
    ),
  );
}

Widget _showTitle() {
  return Container(
    alignment: Alignment.center,
    padding: const EdgeInsets.all(20.0),
    child: Text (
      S.of(context).userTitleText,
      style: Theme.of(context).textTheme.headline6,
    ), 
  );
   // child: Text (
   //   'User Registration',
   //   style: TextStyle(
   //     color: Colors.blue,
   //     fontWeight: FontWeight.w500,
   //     fontSize: 30
   //   )
   // ) 
  //);
}

Widget _showNameInput() {
  return Container(
    alignment: Alignment.center,
    padding: const EdgeInsets.all(10.0),
    child: TextField (      
      //onChanged: (text) => sName=text.trim(),
      // onChanged: (text) {
      //   //sName=text.trim();
      //   oUser.userName = text;
      // },
      controller: nameController,
      maxLines:1,
      maxLength: 60,
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: S.of(context).userNameText,
          hintText: S.of(context).userNameText,
          counterText: "",
          icon: new Icon(
            Icons.person,
            color: Colors.grey,
          )
      ),
      //validator: (value) => value.isEmpty ? 'Email can \'t be empty' : null,
    )
  );
}

Widget _showPass1Input() {
  return Container(
    alignment: Alignment.center,
    padding: const EdgeInsets.all(10.0),
    child: TextField (
      controller: pass1Controller,
      maxLines:1,
      maxLength: 18,
      obscureText: true,
      autofocus: false,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: S.of(context).passwordText,
          hintText: S.of(context).passwordText,
          counterText: "",
          icon: new Icon(
            Icons.lock,
            color: Colors.grey,
          )
      ),
      //validator: (value) => value.isEmpty ? 'Password can \'t be empty' : null,
      //onSaved: (value) => sPass = value.trim(),
    )
  );
}

Widget _showPass2Input() {
  return Container(
    alignment: Alignment.center,
    padding: const EdgeInsets.all(10.0),
    child: TextField (
      controller: pass2Controller,
      maxLines:1,
      maxLength: 18,
      obscureText: true,
      autofocus: false,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: S.of(context).passwordConfirmText,
          hintText: S.of(context).passwordConfirmText,
          counterText: "",
          icon: new Icon(
            Icons.lock,
            color: Colors.grey,
          )
      ),
      //validator: (value) => value.isEmpty ? 'Password can \'t be empty' : null,
      //onSaved: (value) => sPass = value.trim(),
    )
  );
}

Widget _showCheckBoxInput() {
  return Container(
    alignment: Alignment.center,
    padding: const EdgeInsets.all(10.0),
    child: Transform.scale(
      scale: 1.3,
    child: Row (
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget> [
        Checkbox(
          value: this.bEhOwner,
          onChanged: (newValue) {
            setState(() {
                this.bEhOwner = newValue; 
            });
          }
        ),
        Text (
          "Pet Owner",
          textAlign: TextAlign.left,
          style: TextStyle(
            //color: Colors.grey,
            //fontWeight: FontWeight.w200,
            fontSize: 14
          )
        ),

        //Padding(
        //      padding: const EdgeInsets.all(15.0),
        //),
        
        Checkbox(
          value: this.bEhWalker,
          onChanged: (newValue) {
            setState(() {
                this.bEhWalker = newValue; 
            });
          }
        ),
        Text (
          "Pet Walker",
          textAlign: TextAlign.left,
          style: TextStyle(
            //color: Colors.grey,
            //fontWeight: FontWeight.w200,
            fontSize: 14
          )
        ),
      ],
    ),
    ),
  );
}

Widget _showButtons() {
  return Container (
    //height: 100,
    //width: 150,
    alignment: Alignment.center,
    padding: EdgeInsets.all(10.0),
    child: Row (
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget> [
        Expanded(
          child: SizedBox(
          //child: SizedBox(
            height: 70,
            child: RaisedButton (
              padding: EdgeInsets.all(10.0),
              elevation: 5.0,
              //textColor: Colors.blue,
              color: Colors.red,
              shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(50.0),
              ),
              child: new Text(
                S.of(context).buttonCancelText,
                style: new TextStyle(
                  fontSize: 24.0, 
                  color: Colors.white,
                ),
              ),
              onPressed: () {
                //Show the HomeGui
              },
            ),
          ),
        ),
        
        SizedBox(
          width: 20,
        ),
        
        Expanded(
          child: SizedBox(
          //child: SizedBox(
            height: 70,
            child: RaisedButton (
              padding: EdgeInsets.all(10.0),
              elevation: 5.0,
              color: Colors.green,
              shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(50.0),
              ),
              child: new Text(
                S.of(context).buttonCreateText,
                style: new TextStyle(
                  fontSize: 24.0, 
                  //fontWeight: FontWeight.w300,
                  color: Colors.white,
                ),
              ),
              onPressed: () {
                if(nameController.text.isEmpty || 
                  pass1Controller.text.isEmpty ||
                  pass2Controller.text.isEmpty) {
                  return _invalidDialog(S.of(context).invalidDialog4Text);
                } else if(pass1Controller.text != pass2Controller.text) {
                  return _invalidDialog(S.of(context).invalidDialog5Text);
                }
                doUserCreate();
              },
            ),
          ),
        ),
      ],
    ),
  );
}

void doUserCreate() async {
  bool result = await this._masterService.doRegisterUser(UserModel(
    userName: nameController.text,
    userPwd: pass1Controller.text
      ),
    isOwner: this.bEhOwner,
    isWalker: this.bEhWalker);
  if (result == false) { // Nome ja cadastrado
    return _invalidDialog(S.of(context).invalidDialog6Text);
  }
  // Muda para a pagina de cadastro do endereco
  Navigator.pushAndRemoveUntil(context,
    MaterialPageRoute(builder: (context) => UserAddressGui()),
    (Route<dynamic> route) => false,
  );
}

Future _invalidDialog(String message) {
  return showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        content: Text(message, textAlign: TextAlign.center, style: new TextStyle(fontSize: 20.0,
        ),),
      );
    },
  );
}

BottomNavigationBar _showBottomNavigationBar() {
  //final List <IconData> oIconList=[Icons.home, Icons.person, Icons.pets];
  //final List <String> oHintList=["Home", "Profile", "Pet"];
  //final List <bool> oEnableList=[true,false,false];
  //final List <Color> oColorList=[Colors.grey, Color.grey, disabledColor, disabledColor];

  
  return BottomNavigationBar(
    currentIndex: 0,
    type: BottomNavigationBarType.fixed,
    showUnselectedLabels: true,
    items: [
      //BottomNavigationBarItem(icon: 
      //  Icon(
      //    oIconList[0],
      //    Color: oEnableList[0],
      //  ),
      //  title: Text(oHintList[0])
      //),
      BottomNavigationBarItem(icon: Icon(Icons.home), title: Text(S.of(context).buttonNavigationBarHomeText)),
      BottomNavigationBarItem(icon: Icon(Icons.settings), title: Text(S.of(context).buttonNavigationBarSettingText)),
      /*
      BottomNavigationBarItem(icon: Icon(Icons.home), title: Text('Home')),
      BottomNavigationBarItem(icon: Icon(Icons.person), title: Text('Profile')),
      BottomNavigationBarItem(icon: Icon(Icons.pets), title: Text('Pet')),
      BottomNavigationBarItem(icon: Icon(Icons.favorite_border), title: Text('Favorite')),
      BottomNavigationBarItem(icon: Icon(Icons.search), title: Text('Search')),
      BottomNavigationBarItem(icon: Icon(Icons.settings), title: Text('Settings')),
      BottomNavigationBarItem(icon: Icon(Icons.autorenew), title: Text('Switch'))
      */
      //BottomNavigationBarItem(icon: new Icon(Icons.add_a_photo), title: new Text('Photo')),
      //BottomNavigationBarItem(icon: new Icon(Icons.perm_identity), title: new Text('Profile')),
      //BottomNavigationBarItem(icon: new Icon(Icons.favorite_border), title: new Text('Favorite')),
      //BottomNavigationBarItem(icon: new Icon(Icons.date_range), title: new Text('Schedule')),
      //BottomNavigationBarItem(icon: new Icon(Icons.attach_money), title: new Text('Payment')),
      //BottomNavigationBarItem(icon: new Icon(Icons.payment), title: new Text('Payment')),
      //BottomNavigationBarItem(icon: new Icon(Icons.brightness_medium), title: new Text('Bridghtness')),
      //BottomNavigationBarItem(icon: new Icon(Icons.format_size), title: new Text('FontSize')),
      //BottomNavigationBarItem(icon: new Icon(Icons.power_settings_new), title: new Text('Close')),
    ],
    onTap: (iItem) {
      setState(() {
        //
      });         
    },
  );
}

}
