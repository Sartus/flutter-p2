import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pet/view/master_view.dart';
import 'package:pet/view/screen/walker_list_gui.dart';

class WalkerPetListGui extends StatefulWidget {
  final Walker _walker;

  WalkerPetListGui(this._walker);

  @override
  State<StatefulWidget> createState() => new _WalkerPetListGuiState(_walker);
}

class _WalkerPetListGuiState extends State<WalkerPetListGui> {
  final Walker _walker;
  MasterService _masterService = MasterService.instance;
  List<Map<String, dynamic>> maps;

  _WalkerPetListGuiState(this._walker);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Walker Pet List"),
        ),
        // body: _showListView()
        body: FutureBuilder<List<Map<String, dynamic>>>(
          future: _masterService.getByPetByWalkerId(this._walker.id),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              maps = snapshot.data;
              return _showListView();
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          },
        ));
  }

  Widget _showListView() {
    return ListView.builder(
      itemCount: this.maps.length,
      itemBuilder: (context, iPos) {
        return _showCard(iPos);
      },
    );
  }

  Widget _showCard(int iPos) {
    // Walker _oWalker=this.maps[iPos];
    String _sName= this.maps[iPos].values.first;
    String _sNameFirstLetterUpcase=_sName.substring(0,1).toUpperCase();
    // int _iRate=_oWalker.iRate;
    // int _iCurrentColor = iPos.isEven ? 0 : 1 ;

    Widget wAvatar = CircleAvatar(
      // backgroundColor: oColorList[_iCurrentColor],
      child: Text(
        _sNameFirstLetterUpcase,
        // "Text1",
        style: TextStyle(
          color: Colors.white,
          fontSize: 30,
          //fontWeight: FontWeight.bold
        ),
      ),
    );

    Widget wText = Expanded(
      flex: 5,
      child: Row(
        children: <Widget>[
          Text(
            _sName,
            // "Name1",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.w600, fontSize: 20),
          ),
          // Text(
          //   _sName,
          //   // this.maps[iPos].,
          //   // "asas",
          //   maxLines: 1,
          //   overflow: TextOverflow.ellipsis,
          //   style: TextStyle(
          //       color: Colors.grey, fontWeight: FontWeight.w800, fontSize: 12),
          // ),
        ],
      ),
    );

    // Widget wIcon = Expanded (
    //   flex: 5,
    //   child: Row (
    //     children: <Widget> [
    //       Icon(
    //         _iRate >= 1 ? Icons.star : Icons.star_border,
    //       ),
    //       Icon(
    //         _iRate >= 2 ? Icons.star : Icons.star_border,
    //       ),
    //       Icon(
    //         _iRate >= 3 ? Icons.star : Icons.star_border,
    //       ),
    //       Icon(
    //         _iRate >= 4? Icons.star : Icons.star_border,
    //       ),
    //       Icon(
    //         _iRate >= 5 ? Icons.star : Icons.star_border,
    //       ),
    //     ],
    //   ),
    // );

    // Widget wTrailing = Icon(
    //   //Icons.favorite,
    //   // _oWalker.bFavorite ? Icons.favorite : Icons.favorite_border,
    //   color: Colors.black,
    // );

    return Card(
      color: Colors.white,
      elevation: 2.0,
      child: ListTile(
        leading: wAvatar,
        title: Row(children: [
          Expanded(
              child: Row(
            //crossAxisAlignment: CrossAxisAlignment.start ,
            children: <Widget>[
              wText,
              // wIcon,
            ],
          )),
        ]),
        // trailing: wTrailing,
      ),
    );
  }
}
