//https://medium.com/@afegbua/flutter-thursday-13-building-a-user-registration-and-login-process-with-provider-and-external-api-1bb87811fd1d
//https://bendyworks.com/blog/a-month-of-flutter-user-registration-form

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pet/model/pet_pet_model.dart';
import 'package:pet/view/master_view.dart';
//import 'package:pet/view/screen/login_gui.dart';
//import 'package:pet/view/screen/pet_gui.dart';
import 'package:pet/view/screen/user_address_gui.dart';
import 'package:pet/view/screen/user_gui.dart';
import 'package:pet/view/screen/walker_list_gui.dart';
//import 'package:pet/view/screen/pet_gui.dart';
//import 'package:pet/view/screen/walker_list_gui.dart';

import 'package:pet/generated/l10n.dart';

class PetGui extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _State();
}

class _State extends State<PetGui> {
  MasterService _masterService = MasterService.instance;
  final nameController= TextEditingController();
  String pathPhoto;
  int iStatus=-1;
  //bool bIsPetForm;
  //bool bIsLoading;
  File _image;
  final picker = ImagePicker();

  Future getImage(ImageSource src) async {
    final pickedFile = await picker.getImage(source: src);
    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        pathPhoto = pickedFile.path;
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      
      appBar: _appBar(),

      // body: Stack( 
      //   children: <Widget> [
      //     _showForm(),
      //     //_showCircularProgress(),
      //   ],
      // ),
      // body: FutureBuilder<String>(
      //   future: _masterService.getPhoto(),
      //   builder: (context, snapshot) {
      //       if (snapshot.hasData) {
      //         pathPhoto = snapshot.data;
      //         return _showForm();
      //       } else if (snapshot.hasError) {
      //           return Text("${snapshot.error}");
      //       }
      //       return Center(child: CircularProgressIndicator(),);
      //     },
      // ),
      body: _showForm(),

      bottomNavigationBar: _showBottomNavigationBar(),
    );
  }

PreferredSizeWidget _appBar() {
  return AppBar(
    //leading: Icon(Icons.menu),
    leading: new IconButton( 
      icon: new Icon(
        Icons.menu,
        //color: Colors.green[500],
      ),
      onPressed: () {},
    ),    
    title: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget> [
        Icon(
          Icons.pets,
          //color: Colors.green[500],
        ),
        Padding(
          padding: const EdgeInsets.all(10.0),
        ),
        Text (
          "Pet Walking",
        ),
        Padding(
          padding: const EdgeInsets.all(30.0),
        ),
      ],
    ),
  );
}

Widget _showForm() {
  return Container(
    padding: EdgeInsets.all(10.0),
    child: new ListView(
      shrinkWrap: true,
      children: <Widget>[
        _showTitle(),
        _showNameInput(),
        _showPhotoInput(),
        _showPhotoAndGalleryButton(),
        _showStatusInput(),
        _showButtons(),
        //_showErrorMessage(),
      ],
    ),
  );
}


Widget _showTitle() {
  return Container(
  //return Padding(
    alignment: Alignment.center,
    //padding: const EdgeInsets.fromLTRB(10,10,10,0),
    padding: const EdgeInsets.all(20.0),
    //padding: const EdgeInsets.fromLTRB(0.0,100,0.0,0.0),
    //child: TextFormField (
    child: Text (
      S.of(context).petPetRegistrationText,
      style: TextStyle(
        color: Colors.blue,
        fontWeight: FontWeight.w500,
        fontSize: 30
      )
    ) 
  );
}

Widget _showNameInput() {
  return Container(
  //return Padding(
    alignment: Alignment.center,
    //padding: const EdgeInsets.fromLTRB(10,10,10,0),
    padding: const EdgeInsets.all(10.0),
    //padding: const EdgeInsets.fromLTRB(0.0,100,0.0,0.0),
    //child: TextFormField (
    child: TextField (
      controller: nameController,
      maxLines:1,
      maxLength: 24,
      keyboardType: TextInputType.emailAddress,
      autofocus: false,

      //controller: oName,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: S.of(context).petPetNameText,
          hintText: S.of(context).petPetNameText,
          counterText: "",
          icon: new Icon(
            Icons.pets,
            color: Colors.grey,
          )
      ),
      //validator: (value) => value.isEmpty ? 'Email can \'t be empty' : null,
      //onSaved: (value) => sNome = value.trim(),
    )
  );
}

Widget _showPhotoInput() {
  return 
    Center(
      child: _image == null
      ? Icon(
          Icons.camera_alt,
          size: 200,
          color: Colors.grey,
        )
      : Image.file(_image,),
    );
}

Widget _showPhotoAndGalleryButton() {
  return
    Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>
      [
        RaisedButton(
          onPressed: () {
            getImage(ImageSource.camera);
          },
          child: Text('Camera'),
        ),
        RaisedButton(
          onPressed: () {
            getImage(ImageSource.gallery);
          },
          child: Text(S.of(context).petGalleryText),
        ),
      ],
    );
}

//ToDo: Tem de capturar a foto pela camera
// Widget _showPhotoInput() {
//   _masterService.getPhoto().then((value) => this.pathPhoto=value);
//   String _url = "https://images.dog.ceo/breeds/";
//   return Container(
//     alignment: Alignment.center,
//     padding: const EdgeInsets.all(20.0),
//     child: Image(
//       height: 300,
//       width: 300,
//       //fit: BoxFit.contain,
//       fit: BoxFit.cover,
//       // image: AssetImage('assets/images/dog011.jpg')
//       image: NetworkImage(_url + this.pathPhoto)
//     ) 
//   );
// }

Widget _showStatusInput() {
  return Container(
    //
  );
}

Widget _showButtons() {
  return Container (
    //height: 100,
    //width: 150,
    alignment: Alignment.center,
    padding: EdgeInsets.all(10.0),
    child: Row (
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget> [
        Expanded(
          child: SizedBox(
          //child: SizedBox(
            height: 70,
            child: RaisedButton (
              padding: EdgeInsets.all(10.0),
              elevation: 5.0,
              //textColor: Colors.blue,
              color: Colors.red,
              shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(50.0),
              ),
              child: new Text(
                S.of(context).buttonCancelText,
                style: new TextStyle(
                  fontSize: 20.0, 
                  color: Colors.white,
                ),
              ),
              onPressed: () {
                //Show the HomeGui
              },
            ),
          ),
        ),
        SizedBox(
          width: 20,
        ),
        Expanded(
          child: SizedBox(
          //child: SizedBox(
            height: 70,
            child: RaisedButton (
              padding: EdgeInsets.all(10.0),
              elevation: 5.0,
              color: Colors.green,
              shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(50.0),
              ),
              child: new Text(
                S.of(context).buttonCreateText,
                style: new TextStyle(
                  fontSize: 20.0, 
                  //fontWeight: FontWeight.w300,
                  color: Colors.white,
                ),
              ),
              onPressed: () {
                //Show the HomeGui
                if(nameController.text.isEmpty) {
                  return _invalidDialog(S.of(context).invalidDialog1Text);
                }
                doCreatePet();
              },
            ),
          ),
        ),
      ],
    ),
  );
}

void doCreatePet() async {
  bool result = await this._masterService.doRegisterPet(PetModel(petNm: nameController.text,
   relativePathNamePicture: this.pathPhoto));
  if(result == false) {
    return _invalidDialog(S.of(context).invalidDialog2Text);
  }
  // Muda para a pagina de cadastro do endereco
    Navigator.pushAndRemoveUntil(context,
      MaterialPageRoute(builder: (context) => WalkerListGui()),
      (Route<dynamic> route) => false,
    );
}

Future _invalidDialog(String message) {
  return showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        content: Text(message, textAlign: TextAlign.center,
         style: new TextStyle(fontSize: 20.0),
        ),
      );
    },
  );
}

BottomNavigationBar _showBottomNavigationBar() {
  final List <IconData> oIconList=[Icons.home, Icons.person, Icons.edit_location, Icons.settings];
  // final List <String> oHintList=["Home", "Profile", "Address", "Setting"];
  final List <String> oHintList=[
    S.of(context).buttonNavigationBarHomeText,
    S.of(context).buttonNavigationBarProfileText,
    S.of(context).buttonNavigationBarAddressText,
    S.of(context).buttonNavigationBarSettingText
    ];
  //final List <bool> oEnableList=[true,false,false];
  final List <Color> oColorList=[Colors.grey, Colors.black, Colors.black, Colors.grey];
  
  return BottomNavigationBar(
    currentIndex: 1,
    type: BottomNavigationBarType.fixed,
    showUnselectedLabels: true,
    items: [
      //BottomNavigationBarItem(icon: 
      //  Icon(
      //    oIconList[0],
      //    Color: oEnableList[0],
      //  ),
      //  title: Text(oHintList[0])
      //),
      BottomNavigationBarItem(
        icon: Icon(
          oIconList[0],
          color: oColorList[0],
        ), 
        title: Text(
          oHintList[0]
        )
      ),
      BottomNavigationBarItem(
        icon: Icon(
          oIconList[1],
          color: oColorList[1],
        ), 
        title: Text(
          oHintList[1]
        )
      ),
      //BottomNavigationBarItem(icon: Icon(Icons.person), title: Text('Profile')),
      BottomNavigationBarItem(
        icon: Icon(
          oIconList[2],
          color: oColorList[2],
        ), 
        title: Text(
          oHintList[2]
        )
      ),
      BottomNavigationBarItem(
        icon: Icon(
          oIconList[3],
          color: oColorList[3],
        ), 
        title: Text(
          oHintList[3]
        )
      ),
      /*
      BottomNavigationBarItem(icon: Icon(Icons.home), title: Text('Home')),
      BottomNavigationBarItem(icon: Icon(Icons.person), title: Text('Profile')),
      BottomNavigationBarItem(icon: Icon(Icons.pets), title: Text('Pet')),
      BottomNavigationBarItem(icon: Icon(Icons.favorite_border), title: Text('Favorite')),
      BottomNavigationBarItem(icon: Icon(Icons.search), title: Text('Search')),
      BottomNavigationBarItem(icon: Icon(Icons.settings), title: Text('Settings')),
      BottomNavigationBarItem(icon: Icon(Icons.autorenew), title: Text('Switch'))
      */
      //BottomNavigationBarItem(icon: new Icon(Icons.add_a_photo), title: new Text('Photo')),
      //BottomNavigationBarItem(icon: new Icon(Icons.perm_identity), title: new Text('Profile')),
      //BottomNavigationBarItem(icon: new Icon(Icons.favorite_border), title: new Text('Favorite')),
      //BottomNavigationBarItem(icon: new Icon(Icons.date_range), title: new Text('Schedule')),
      //BottomNavigationBarItem(icon: new Icon(Icons.attach_money), title: new Text('Payment')),
      //BottomNavigationBarItem(icon: new Icon(Icons.payment), title: new Text('Payment')),
      //BottomNavigationBarItem(icon: new Icon(Icons.brightness_medium), title: new Text('Bridghtness')),
      //BottomNavigationBarItem(icon: new Icon(Icons.format_size), title: new Text('FontSize')),
      //BottomNavigationBarItem(icon: new Icon(Icons.power_settings_new), title: new Text('Close')),
    ],
    onTap: (iItem) {
      setState(() {
        if ( (iItem==0) || (iItem==3) ) return;
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            //builder: (context) => HomeGui()
            builder: (context) {
              if (iItem==1) return UserGui();
              if (iItem==2) return UserAddressGui();
              return PetGui();
            }
          ),
          (Route<dynamic> route) => true,
        );
      });         
    },
  );
}

}
