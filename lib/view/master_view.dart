

import 'package:pet/common/commmon.dart';
import 'package:pet/controller/ad_address_controller.dart';
import 'package:pet/controller/pet_owner_controller.dart';
import 'package:pet/controller/pet_pet_controller.dart';
import 'package:pet/controller/pet_pet_vs_pet_owner_controller.dart';
import 'package:pet/controller/pet_picture_controller.dart';
import 'package:pet/controller/pet_vs_pet_walker_controller.dart';
import 'package:pet/controller/pet_walker_controller.dart';
import 'package:pet/controller/us_user_controller.dart';
import 'package:pet/controller/us_user_vs_address_controller.dart';
import 'package:pet/model/ad_address_model.dart';
import 'package:pet/model/pet_owner_model.dart';
import 'package:pet/model/pet_pet_model.dart';
import 'package:pet/model/pet_vs_pet_owner_model.dart';
import 'package:pet/model/pet_walker_model.dart';
import 'package:pet/model/us_user_model.dart';
import 'package:pet/model/us_user_vs_address_model.dart';
import 'package:pet/services/NotificationPlugin.dart';
import 'package:pet/services/firebase/firebase_conection.dart';

class MasterService {
  // UserModel _activeUser;
  // PetWalkerModel _activeWalker;
  // PetOwnerModel _activeOwner;
  String _activeUserId;
  String _activeWalker;
  String _activeOwner;

  AddressModel address;
  final List<PetWalkerModel> _walkerList = [];
  final List<PetModel> _petList = [];
  // final List<String> _pictureList = [];
  final EnumSideType _type;

  // torna esta classe singleton
  MasterService._privateConstructor({EnumSideType type = EnumSideType.server}) :
  _type = type;
  static final MasterService instance = MasterService._privateConstructor();

  // MasterService({EnumSideType type = EnumSideType.server}) :
  //   _type = type;

  Future<int> _cadastraPet(PetModel model) {
    PetController crtl = PetController(type: EnumSideType.server);
    return crtl.insert(model);
  }

  // Faz insert na tabela PetVsPetOwnerModel
  Future<int> _cadastraPetVsPetOwner(PetVsPetOwnerModel model) {
    PetVsPetOwnerController crtl = PetVsPetOwnerController(type: EnumSideType.server);
    return crtl.insert(model);
  }

  // Faz insert na tabela UserModel
  Future<int> _cadastraUser(UserModel model) {
    UserController crtl = UserController(type: _type);
    return crtl.insert(model);
  }

  // Faz insert na tabela PetOwnerModel
  Future<int> _cadastraPetOwner(PetOwnerModel model) {
    PetOwnerController crtl = PetOwnerController(type: EnumSideType.server);
    return crtl.insert(model);
  }

  // Faz insert na tabela PetWalkerModel
  Future<int> _cadastraPetWalker(PetWalkerModel model) {
    PetWalkerController crtl = PetWalkerController(type: EnumSideType.server);
    return crtl.insert(model);
  }

  // Faz insert na tabela AddressModel
  Future<int> _cadastraAddress(AddressModel model) {
    AddressController crtl = AddressController(type: EnumSideType.server);
    return crtl.insert(model);
  }

  // Faz insert na tabela UserVsAddressModel
  Future<int> _cadastraUserVsAddress(UserVsAddressModel model) {
    UserVsAddressController crtl = UserVsAddressController(type: EnumSideType.server);
    return crtl.insert(model);
  }

  // Devolve o usuario ativo
  String get activeUser => _activeUserId;

  // Devolve o PetOwnerModel pelo idUser passado.
  Future<PetOwnerModel> getOwnerByUserId(int idUser) async {
    PetOwnerController ctrl = PetOwnerController(type: EnumSideType.server);
    return await ctrl.getByUserId(idUser);
  }

  // Devolve o PetWalkerModel pelo idUser passado.
  Future<PetWalkerModel> getWalkerByUserId(int idUser) async {
    PetWalkerController ctrl = PetWalkerController(type: EnumSideType.server);
    return await ctrl.getByUserId(idUser);
  }

  // Devolve o PetModel pelo idUser passado.
  Future<PetModel> getPetByUserId(int idUser) async {
    PetController ctrl = PetController(type: EnumSideType.server);
    return await ctrl.getByUserId(idUser);
  }

  // Devolve o AddressModel pelo idUser passado.
  Future<AddressModel> getAddressByUserId(int idUser) async {
    AddressController ctrl = AddressController(type: EnumSideType.server);
    return await ctrl.getByUserId(idUser);
  }

  // Devolve o nome do usuario e o endereco pelo WalkerId passado.
  Future<Map<String, dynamic>> getByWalkerId(int id) async {
    PetWalkerController ctrl = PetWalkerController(type: EnumSideType.server);
    return await ctrl.getByPetWalkerId(id);
  }

  // Devolve o nome do pet pelo WalkerId passado.
  Future<List<Map<String, dynamic>>> getByPetByWalkerId(int id) async {
    PetWalkerController ctrl = PetWalkerController(type: EnumSideType.server);
    return await ctrl.getPetByWalkerId(id);
  }

  // Faz a validacao e login do usuario
  Future<bool> doLogin(UserModel model) async {
    Map<String, dynamic> result = 
      await firebaseConection.getUserByName(model.userName);
    // if(map == null) {
    if(result.isEmpty) {
      return false;
    } else if (result.values.first['senha'] == model.userPwd) {
      _activeUserId = result.keys.first;
      return true;
    }
    return false;
  }

  // Faz o registro do usuario.
  Future<bool> doRegisterUser(UserModel model, {bool isOwner, bool isWalker}) async {
    Map<String, dynamic> map =
      await firebaseConection.getUserByName(model.userName);
    if(map.isEmpty) { // User nao existe
      Map<String, dynamic> resultUser = 
        await firebaseConection.cadastraUser({
          'nome': model.userName,
          'senha': model.userPwd
        });
        _activeUserId = resultUser.keys.first;
      if (isOwner) {
        Map<String, dynamic> resultOwner = 
        await firebaseConection.cadastraPetOwner({
          'userId': resultUser.values.first
        });
      }
      if(isWalker) {
        Map<String, dynamic> resultWalker = 
        await firebaseConection.cadastraPetWalker({
          'userId': resultUser.values.first
        });
      }
      notificationPlugin.showNotification("Bem-Vindo", "Seja bem-vindo ao app Auau.");
      return true;
    }
    return false;
  }

  // Faz o cadastro do endereco do usuario
  Future<bool> doRegisterAddress(AddressModel model, {String number, String complementary}) async {
    Map<String, dynamic> result = 
      await firebaseConection.cadastraAddress(model.toMap());
    if(result.isNotEmpty) {
      await firebaseConection.cadastraUserVsAddress({
        "userId": _activeUserId,
        "addressId": result.values.first
      });
      return true;
    }
    return false;
  }

  // Faz o matchmaker pelo idUser
  Future<List<PetWalkerModel>> getMatchMakerByUserId(int idUser) async {
    UserVsAddressController crtl = UserVsAddressController(type: EnumSideType.server);
    List<Map<String, dynamic>> maps =  await crtl.matchMaker(UserModel(idUser: idUser));
    maps.forEach((element) => _walkerList.add(PetWalkerModel.fromMap(element)));
    return _walkerList;
  }

  // Faz o cadastro do pet
  Future<bool> doRegisterPet(PetModel model) async {
    Map<String, dynamic> result = 
      await firebaseConection.cadastraPet({
        "petNm": model.petNm,
        "relativePathNamePicture": model.relativePathNamePicture
      });
    if(result.isNotEmpty) {
      if(_activeOwner == null) {
        Map<String, dynamic> resultOwner = await firebaseConection.rawGetBy(
          "PetOwners", "userId", _activeUserId);
        if(resultOwner.isEmpty) {
          Map<String, dynamic> resultOwner = 
            await firebaseConection.cadastraPetOwner({
            "userId": _activeUserId
            });
          _activeOwner = resultOwner.values.first;
        } else {
          // _activeOwner = resultOwner.values.first;
        }
      }
      Map<String, dynamic> resultPetVsPetOwner = 
        await firebaseConection.cadastraPetVsPetOwner({
        "petId": result.values.first,
        "petOwnerId": _activeOwner
      });
      return true;
    }
    return false;
  }

  // Devolve o caminho relativo da imagem.
  Future<String> getPhoto() async {
    PetPictureController ctrl = PetPictureController(type: EnumSideType.server);
    String image;
    await ctrl.getImage().then((value) => image = value);
    return image;
  }

  // Devolve todos os walkers cadastrados
  Future<List<PetWalkerModel>> getWalkers() async {
    PetWalkerController ctrl = PetWalkerController(type: EnumSideType.server);
    return await ctrl.getWalkerList();
  }

  // Devolve o UserModel pelo userId passado
  Future<UserModel> getUserByUserId(int id) async {
    UserController ctrl = UserController(type: EnumSideType.server);
    return await ctrl.getById(id);
  }

  // Devolve o nome do usuario de acordo userId passado
  Future<String> getUserNameByUserId(int id) async {
    UserController ctrl = UserController(type: EnumSideType.server);
    String name;
    await ctrl.getById(id).then((value) => name = value.userName);
    return name;
  }

  // Devolve todos os PetWalkerId distintos que estao com pets cadastrados
  Future<List<int>> getDistinctPetWalkerId() async {
    final PetVsPetWalkerController  ctrl = PetVsPetWalkerController(type: EnumSideType.server);
    return await ctrl.getDistinctPetWalkerId();
  }

  // Devolve o PetWalkerModel referente ao userId passado.
  Future<PetWalkerModel> getWalkerById(int id) async {
    final PetWalkerController  ctrl = PetWalkerController(type: EnumSideType.server);
    return await ctrl.getById(id);
  }
}