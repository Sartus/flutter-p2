import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'package:pet/generated/l10n.dart';
import 'package:pet/view/screen/login_gui.dart';
// import 'package:pet/view/screen/pet_gui.dart';
// import 'package:pet/view/screen/pet_gui.dart';
// import 'package:pet/view/screen/user_address_gui.dart';
// import 'package:pet/view/screen/user_gui.dart';
// import 'package:pet/view/screen/walker_detail_gui.dart';
// import 'package:pet/view/screen/walker_list_gui.dart';

void main() async {
  //await DB.init();
  runApp(PetApp());
}

class PetApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Pet Walking - App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        textTheme: TextTheme(
          headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
          headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic, color: Colors.orange),
          // bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
          bodyText2: TextStyle(fontSize: 16.0),
        ),
      ),
      localizationsDelegates:
      [
        // 1
        S.delegate,
        // 2
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: S.delegate.supportedLocales,
  //  home: UserGui()
  //  home: UserAddressGui()
  //  home: PetGui()
      home: LoginGui()
  //  home: WalkerListGui()
    );
  }
}