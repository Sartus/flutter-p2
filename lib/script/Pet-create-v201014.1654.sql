CREATE TABLE `PET_Pet` (
  `IdPet` integer PRIMARY KEY AUTOINCREMENT,
  `PetStatusTpId` integer,
  `PetNm` varchar(25),
  `RelativePathNamePicture` varchar(60), -- URL REST API
  -- `RelativePathNameJson` varchar(60),
  FOREIGN KEY (`PetStatusTpId`)
   REFERENCES `PET_PetStatusTp` (`IdPetStatusTp`) ON DELETE CASCADE
);

-- CREATE TABLE `PET_PetAdditional` (
--   `IdPetAdditiona` integer PRIMARY KEY AUTOINCREMENT,
--   `PetId` integer,
--   `NoteDs` varchar(50),
--   FOREIGN KEY (`PetId`)
--    REFERENCES `PET_Pet` (`IdPet`)
-- );

CREATE TABLE `PET_PetPicture` (
  `IdPetPicture` integer PRIMARY KEY AUTOINCREMENT,
  `PetId` integer,
  `PictureSq` integer,
  `PictureRelativePath` varchar(60),
  `NoteDs` varchar(50),
  FOREIGN KEY (`PetId`)
   REFERENCES `PET_Pet` (`IdPet`) ON DELETE CASCADE
);

CREATE TABLE `PET_PetPedigree` (
  `IdPetPedigree` integer PRIMARY KEY AUTOINCREMENT,
  `PetId` integer,
  `PetBreedTpId` integer,
  `PedigreeNr` varchar(50),
  `PictureRelativePath` varchar(60),
  FOREIGN KEY (`PetId`)
   REFERENCES `PET_Pet` (`IdPet`) ON DELETE CASCADE,
  FOREIGN KEY (`PetBreedTpId`)
   REFERENCES `PET_PetBreedTp` (`IdPetBreedTp`) ON DELETE CASCADE
);

CREATE TABLE `PET_PetOwner` (
  `IdPetOwner` integer PRIMARY KEY AUTOINCREMENT,
  `UserId` integer,
  `PetStatusTpId` integer,
  FOREIGN KEY (`UserId`) 
    REFERENCES `US_User` (`IdUser`) ON DELETE CASCADE,
  FOREIGN KEY (`PetStatusTpId`)
   REFERENCES `PET_PetStatusTp` (`IdPetStatusTp`) ON DELETE CASCADE
);

CREATE TABLE `PET_PetWalker` (
  `IdPetWalker` integer PRIMARY KEY AUTOINCREMENT,
  `UserId` integer,
  `PetStatusTpId` integer,
  `RateNr` integer,
  FOREIGN KEY (`UserId`) 
    REFERENCES `US_User` (`IdUser`) ON DELETE CASCADE,
  FOREIGN KEY (`PetStatusTpId`)
    REFERENCES `PET_PetStatusTp` (`IdPetStatusTp`) ON DELETE CASCADE
);

CREATE TABLE `PET_PetVsPetOwner` (
  `IdPetVsPetOwner` integer PRIMARY KEY AUTOINCREMENT,
  `PetId` integer,
  `PetOwnerId` integer,
  `PetStatusTpId` integer,
  FOREIGN KEY (`PetId`) 
    REFERENCES `PET_Pet` (`IdPet`) ON DELETE CASCADE,
  FOREIGN KEY (`PetOwnerId`)
   REFERENCES `PET_PetOwner` (`IdPetOwner`) ON DELETE CASCADE,
  FOREIGN KEY (`PetStatusTpId`)
   REFERENCES `PET_PetStatusTp` (`IdPetStatusTp`) ON DELETE CASCADE
);

CREATE TABLE `PET_PetVsPetWalker` (
  `IdPetVsPetWalker` integer PRIMARY KEY AUTOINCREMENT,
  `PetId` integer,
  `PetWalkerId` integer,
  `PetStatusTpId` integer,
  `IsFavority` bool,
  FOREIGN KEY (`PetId`)
   REFERENCES `PET_Pet` (`IdPet`) ON DELETE CASCADE,
  FOREIGN KEY (`PetWalkerId`)
   REFERENCES `PET_PetWalker` (`IdPetWalker`) ON DELETE CASCADE,
  FOREIGN KEY (`PetStatusTpId`)
   REFERENCES `PET_PetStatusTp` (`IdPetStatusTp`) ON DELETE CASCADE
);

CREATE TABLE `PET_PetBreedTp` (
  `IdPetBreedTp` integer PRIMARY KEY AUTOINCREMENT,
  `PetBreedTpCd` varchar(2),
  `PetBreedTpAc` varchar(5),
  `PetBreedTpDs` varchar(50),
  `OwnerId` integer,
  FOREIGN KEY (`OwnerId`)
   REFERENCES `PET_PetBreedTp` (`IdPetBreedTp`) ON DELETE CASCADE
);

CREATE TABLE `PET_PetSizeTp` (
  `IdPetSizeTp` integer PRIMARY KEY AUTOINCREMENT,
  `PetSizeTpCd` varchar(2),
  `PetSizeTpAc` varchar(5),
  `PetSizeTpDs` varchar(50)
);

CREATE TABLE `PET_PetColorTp` (
  `IdPetColorStatusTp` integer PRIMARY KEY AUTOINCREMENT,
  `PetColorTpCd` varchar(2),
  `PetColorTpAc` varchar(5),
  `PetColorTpDs` varchar(50)
);

CREATE TABLE `PET_PetHealthTp` (
  `IdPetHealthTp` integer PRIMARY KEY AUTOINCREMENT,
  `PetHealthTpCd` varchar(2),
  `PetHealthTpAc` varchar(5),
  `PetHealthTpDs` varchar(50)
);

CREATE TABLE `PET_PetSpecialCareTp` (
  `IdPetSpecialCareTp` integer PRIMARY KEY AUTOINCREMENT,
  `PetSpecialCareTpCd` varchar(2),
  `PetSpecialCareTpAc` varchar(5),
  `PetSpecialCareTpDs` varchar(50)
);

CREATE TABLE `PET_PetFavoritePlayTp` (
  `IdPetFavoritePlayTp` integer PRIMARY KEY AUTOINCREMENT,
  `PetFavoritePlayTpCd` varchar(2),
  `PetFavoritePlayTpAc` varchar(5),
  `PetFavoritePlayTpDs` varchar(50)
);

CREATE TABLE `PET_PetToolsTp` (
  `IdPetToolsTp` integer PRIMARY KEY AUTOINCREMENT,
  `PetToolsTpCd` varchar(2),
  `PetToolsTpAc` varchar(5),
  `PetToolsTpDs` varchar(50)
);

CREATE TABLE `PET_PetStatusTp` (
  `IdPetStatusTp` integer PRIMARY KEY AUTOINCREMENT,
  `PetStatusTpCd` varchar(2),
  `PetStatusTpAc` varchar(5),
  `PetStatusTpDs` varchar(25)
);

--

CREATE TABLE `US_User` (
  `IdUser` integer PRIMARY KEY AUTOINCREMENT,
  `UserName`  varchar(60),
  `UserPwd` varchar(18),
  `PetStatusTpId` integer,
  `RelativePathNamePicture` varchar(60),
  -- `RelativePathNameJson` varchar(60),
  FOREIGN KEY (`PetStatusTpId`)
   REFERENCES `PET_PetStatusTp` (`IdPetStatusTp`) ON DELETE CASCADE
);

CREATE TABLE `AD_Address` (
  `IdAddress` integer PRIMARY KEY AUTOINCREMENT,
  -- `UserId` integer,
  `Neighborhood` varchar(50),
  `City` varchar(50),
  `State` varchar(255),
  --`AddressTpId` integer,
  `Address` varchar(255),
  -- `Number` varchar(20),
  -- `Complementary` varchar(50),
  `ZipCode` varchar(9),
  `Latitude` REAL,
  `Longitude` REAL
  -- FOREIGN KEY (`UserId`)
  --  REFERENCES `US_User` (`IdUser`)
);

CREATE TABLE `US_UserVsAddress` (
  `IdUserVsAddress` integer PRIMARY KEY AUTOINCREMENT,
  `UserId` integer,
  `AddressId` integer,
  -- `Neighborhood` varchar(50),
  -- `City` varchar(50),
  -- `State` varchar(255),
  --`AddressTpId` integer,
  -- `Address` varchar(255),
  `Number` varchar(10),
  `Complementary` varchar(10),
  -- `ZipCode` varchar(9),
  -- `Latitude` REAL,
  -- `Longitude` REAL,
  FOREIGN KEY (`UserId`)
    REFERENCES `US_User` (`IdUser`) ON DELETE CASCADE
  FOREIGN KEY (`AddressId`)
    REFERENCES `AD_Address` (`IdAddress`) ON DELETE CASCADE
);

-- CREATE TABLE `PET_PetWalkerVsAddress` (
--   `IdWalkerVsAddress` integer PRIMARY KEY AUTOINCREMENT,
--   `UserId` integer,
--   `AddressId` integer,
--   -- `Neighborhood` varchar(50),
--   -- `City` varchar(50),
--   -- `State` varchar(255),
--   --`AddressTpId` integer,
--   -- `Address` varchar(255),
--   `Number` varchar(20),
--   `Complementary` varchar(50),
--   -- `ZipCode` varchar(9),
--   -- `Latitude` REAL,
--   -- `Longitude` REAL,
--   FOREIGN KEY (`UserId`)
--     REFERENCES `US_User` (`IdUser`)
--   FOREIGN KEY (`AddressId`)
--     REFERENCES `AD_Address` (`IdAddress`)
-- );

CREATE TABLE `SC_UserSchedule` (
  `IdUserSchedule` integer PRIMARY KEY AUTOINCREMENT,
  `UserId` integer,
  -- `ScheduleAdditionalId` integer,
  `BeginTs` DATETIME,
  `EndTs` DATETIME,
  `Title` varchar(50),
   FOREIGN KEY (`UserId`)
    REFERENCES `US_User` (`IdUser`) ON DELETE CASCADE
);

-- CREATE TABLE `SC_Schedule_Walker` (
--   `IdSchedule` integer PRIMARY KEY AUTOINCREMENT,
--   `UserId` integer,
--   `ScheduleAdditionalId` integer,
--   `BeginTs` DATETIME,
--   `EndTs` DATETIME,
--   `Title` varchar(50),
--   FOREIGN KEY (`UserId`)
--     REFERENCES `US_User` (`IdUser`)
-- );

-- CREATE TABLE `SC_UserScheduleAdditional` (
--   `IdScheduleAdditional` integer PRIMARY KEY AUTOINCREMENT,
--   `UserScheduleId` int,
--   `ScheduleAdditional` varchar(50),
--   FOREIGN KEY (`UserScheduleId`)
--    REFERENCES `SC_UserSchedule` (`IdUserSchedule`)
-- );

-- CREATE TABLE `SC_ScheduleAdditional_Walker` (
--   `IdScheduleAdditional` integer PRIMARY KEY AUTOINCREMENT,
--   `ScheduleId` int,
--   `ScheduleAdditional` varchar(50),
--   FOREIGN KEY (`ScheduleId`)
--    REFERENCES `SC_Schedule` (`IdSchedule`)
-- );