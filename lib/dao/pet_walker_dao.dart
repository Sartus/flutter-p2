

import 'package:pet/common/commmon.dart';
import 'package:pet/dao/base_dao.dart';
import 'package:pet/db/client_conn_db.dart';
import 'package:pet/db/server_conn_db.dart';

class PetWalkerDao extends BaseDao{
  static const String _nameTable = "`PET_PetWalker`";

  PetWalkerDao({EnumSideType type = EnumSideType.client}) : super(
    (type == EnumSideType.client) ? 
    ClientDatabase.instance.database :
    ServerDatabase.instance.database,
    _nameTable);

  Future<Map<String, dynamic>> queryByUserId(var model) async {
    int id = model.userId;
    String raw = "SELECT * \n" + 
    "FROM PET_PetWalker \n" +
    "WHERE PET_PetWalker.UserId = $id;";

    List<Map<String, dynamic>> maps = await super.rawQuery(raw);
    
    return maps.length > 0 ? maps.first : null;
  }

  Future<Map<String, dynamic>> queryByPetWalkerId(var model) async {
    int id = model.idPetWalker;
    String raw = "Select US_User.UserName, AD_Address.Address\n" + 
    "From US_UserVsAddress, PET_PetWalker, AD_Address, US_User \n" +
    "Where  PET_PetWalker.IdPetWalker = $id; \n" +
    "AND PET_PetWalker.UserId = US_UserVsAddress.UserId \n" +
    "AND PET_PetWalker.UserId = US_User.IdUser \n" +
    "AND PET_PetWalker.UserId = US_UserVsAddress.UserId \n" +
    "AND US_UserVsAddress.AddressId = AD_Address.IdAddress; \n";

    List<Map<String, dynamic>> maps = await super.rawQuery(raw);
    
    return maps.length > 0 ? maps.first : null;
  }

  Future<List<Map<String, dynamic>>> queryPetByPetWalkerId(var model) async {
    int id = model.idPetWalker;
    String raw = "Select PET_Pet.PetNm \n" + 
    "From PET_PetVsPetWalker, PET_Pet \n" +
    "Where PET_PetVsPetWalker.PetWalkerId = $id \n" +
    "AND PET_PetVsPetWalker.PetId = PET_Pet.IdPet \n" + 
    "LIMIT 8;";
    return await super.rawQuery(raw);
  }
}