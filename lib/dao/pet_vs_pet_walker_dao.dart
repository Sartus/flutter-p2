

import 'package:pet/common/commmon.dart';
import 'package:pet/dao/base_dao.dart';
import 'package:pet/db/client_conn_db.dart';
import 'package:pet/db/server_conn_db.dart';

class PetVsPetWalkerDao extends BaseDao{
  static const String _nameTable = "`PET_PetVsPetWalker`";

  PetVsPetWalkerDao({EnumSideType type = EnumSideType.client}) : super(
    (type == EnumSideType.client) ? 
    ClientDatabase.instance.database :
    ServerDatabase.instance.database,
    _nameTable);

  Future<List<Map<String, dynamic>>> queryDistinctPetWalkerId() async {
    String raw = "SELECT DISTINCT PET_PetVsPetWalker.PetWalkerId \n" +
    "from PET_PetVsPetWalker \n" + 
    "ORDER BY PET_PetVsPetWalker.PetWalkerId";
    return await super.rawQuery(raw);
  }
}