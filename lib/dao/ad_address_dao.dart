

import 'package:pet/common/commmon.dart';
import 'package:pet/dao/base_dao.dart';
import 'package:pet/db/client_conn_db.dart';
import 'package:pet/db/server_conn_db.dart';

class AddressDao extends BaseDao{
  static const String _nameTable = "`AD_Address`";

  AddressDao({EnumSideType type = EnumSideType.client}) : super(
    (type == EnumSideType.client) ? 
    ClientDatabase.instance.database :
    ServerDatabase.instance.database,
    _nameTable);

  Future<Map<String, dynamic>> queryByUserId(var model) async {
    int id = model.userId;
    String raw = "SELECT * \n" + 
    "FROM AD_Address, US_UserVsAddress \n" +
    "WHERE US_UserVsAddress.UserId = $id;";
    List<Map<String, dynamic>> maps = await super.rawQuery(raw);
    return maps.length > 0 ? maps.first : null;
  }
}