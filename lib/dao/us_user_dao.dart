

import 'package:pet/common/commmon.dart';
import 'package:pet/dao/base_dao.dart';
import 'package:pet/db/client_conn_db.dart';
import 'package:pet/db/server_conn_db.dart';

class UserDao extends BaseDao{
  static const String _nameTable = "`US_User`";

  UserDao({EnumSideType type = EnumSideType.client}) : super(
    (type == EnumSideType.client) ? 
    ClientDatabase.instance.database :
    ServerDatabase.instance.database,
    _nameTable);

  Future<Map<String, dynamic>> queryByName(var model) async {
    String name = model.userName;
    String raw = "Select * \n" + "From US_User \n" + 
    "Where US_User.UserName = '$name';";

    List<Map<String, dynamic>> maps = await super.rawQuery(raw);
    
    return maps.length > 0 ? maps.first : null;
  }
}