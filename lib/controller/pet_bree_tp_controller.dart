

import 'package:flutter/material.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/dao/pet_bree_tp_dao.dart';
import 'package:pet/model/pet_bree_tp_model.dart';

class PetBreedTpController {
  final PetBreedTpDao _breedDao;
  final List<PetBreedTpModel> _breedList  = [];
  // final Widget _breedGui;
  // final EnumSideType _type;

  PetBreedTpController({EnumSideType type = EnumSideType.client, Widget gui}) :
    // _type = type,
    _breedDao = PetBreedTpDao(type: type);
    // _breedGui = gui;
  
  Future<List<PetBreedTpModel>> getBreedList () async { 
    return _breedList.isEmpty? getAllRows() : _breedList;
  }

  Future<int> insert(PetBreedTpModel obj) async {
    // result é o conteudo da PK da linha inserida.
    int result;
    try {
      result = await _breedDao.insert(obj);
    } catch(e) {
      return 0;
    }
    _breedList.add(obj);
    return result;
  }

  Future<int> update(PetBreedTpModel obj) async {
    int result = await _breedDao.update(obj);
    if (result > 0) {
      _breedList.add(obj);
    }
    return result;
  }

  Future<List<PetBreedTpModel>> getAllRows() async {
    List<Map<String, dynamic>> maps =  await _breedDao.queryAllRows();
    _breedList.clear();
    maps.forEach((map) => _breedList.add(PetBreedTpModel.fromMap(map)));
    return _breedList;
  }

  Future<List<PetBreedTpModel>> getRows({int limit = 10, int offset = 0}) async {
    List<Map<String, dynamic>> maps =  await _breedDao.queryRows(limit, offset);
    _breedList.clear();
    maps.forEach((map) => _breedList.add(PetBreedTpModel.fromMap(map)));
    return _breedList;
  }

  // Retorna a linha que contem o id do model passado. Caso nao encontre
  // retorna null
  Future<PetBreedTpModel> getById(int id) async {
    return _breedDao.queryById(PetBreedTpModel(idPetBreedTp: id)).then((map) => PetBreedTpModel.fromMap(map));
  }

  Future<int> delete(int id) async {
    int result = await _breedDao.delete(PetBreedTpModel(idPetBreedTp: id));
    _breedList.removeWhere((item) => item.idPetBreedTp == id);
    return result;
  }
}