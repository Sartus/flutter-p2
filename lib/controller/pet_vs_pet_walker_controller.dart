

import 'package:flutter/material.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/dao/pet_vs_pet_walker_dao.dart';
import 'package:pet/model/pet_vs_pet_walker_model.dart';

class PetVsPetWalkerController {
  final PetVsPetWalkerDao _petVsWalkerDao;
  final List<PetVsPetWalkerModel> _petVsWalkerList  = [];
  // final Widget _petVsWalkerGui;
  // final EnumSideType _type;

  PetVsPetWalkerController({EnumSideType type = EnumSideType.client, Widget gui}) :
    // _type = type,
    _petVsWalkerDao = PetVsPetWalkerDao(type: type);
    // _petVsWalkerGui = gui;
  
  Future<List<PetVsPetWalkerModel>> getAddressList () async { 
    return _petVsWalkerList.isEmpty? getAllRows() : _petVsWalkerList;
  }

  Future<int> insert(PetVsPetWalkerModel obj) async {
    // result é o conteudo da PK da linha inserida.
    int result;
    try {
      result = await _petVsWalkerDao.insert(obj);
    } catch(e) {
      return 0;
    }
    _petVsWalkerList.add(obj);
    return result;
  }

  Future<int> update(PetVsPetWalkerModel obj) async {
    int result = await _petVsWalkerDao.update(obj);
    if (result > 0) {
      _petVsWalkerList.add(obj);
    }
    return result;
  }

  Future<List<PetVsPetWalkerModel>> getAllRows() async {
    List<Map<String, dynamic>> maps =  await _petVsWalkerDao.queryAllRows();
    _petVsWalkerList.clear();
    maps.forEach((map) => _petVsWalkerList.add(PetVsPetWalkerModel.fromMap(map)));
    return _petVsWalkerList;
  }

  Future<List<PetVsPetWalkerModel>> getRows({int limit = 10, int offset = 0}) async {
    List<Map<String, dynamic>> maps =  await _petVsWalkerDao.queryRows(limit, offset);
    // print(maps);
    _petVsWalkerList.clear();
    maps.forEach((map) => _petVsWalkerList.add(PetVsPetWalkerModel.fromMap(map)));
    return _petVsWalkerList;
  }

  // Retorna a linha que contem o id do model passado. Caso nao encontre
  // retorna null
  Future<PetVsPetWalkerModel> getById(int id) async {
    return _petVsWalkerDao.queryById(PetVsPetWalkerModel(idPetVsPetWalker: id)).then((map) => PetVsPetWalkerModel.fromMap(map));
  }

  Future<List<int>> getDistinctPetWalkerId() async {
    List<int> lst = [];
    await _petVsWalkerDao.queryDistinctPetWalkerId().then((value) =>
      value.forEach((element) => lst.add(element.values.first)));
    return lst;
  }

  Future<int> delete(int id) async {
    int result = await _petVsWalkerDao.delete(PetVsPetWalkerModel(idPetVsPetWalker: id));
    _petVsWalkerList.removeWhere((item) => item.idPetVsPetWalker == id);
    return result;
  }
}