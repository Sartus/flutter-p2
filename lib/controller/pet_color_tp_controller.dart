

import 'package:flutter/material.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/dao/pet_color_tp_dao.dart';
import 'package:pet/model/pet_color_tp_model.dart';

class PetColorTpController {
  final PetColorTpDao _colorDao;
  final List<PetColorTpModel> _colorList  = [];
  // final Widget _colorGui;
  // final EnumSideType _type;

  PetColorTpController({EnumSideType type = EnumSideType.client, Widget gui}) :
    // _type = type,
    _colorDao = PetColorTpDao(type: type);
    // _colorGui = gui;
  
  Future<List<PetColorTpModel>> getAddressList () async { 
    return _colorList.isEmpty? getAllRows() : _colorList;
  }

  Future<int> insert(PetColorTpModel obj) async {
    // result é o conteudo da PK da linha inserida.
    int result;
    try {
      result = await _colorDao.insert(obj);
    } catch(e) {
      return 0;
    }
    _colorList.add(obj);
    return result;
  }

  Future<int> update(PetColorTpModel obj) async {
    int result = await _colorDao.update(obj);
    if (result > 0) {
      _colorList.add(obj);
    }
    return result;
  }

  Future<List<PetColorTpModel>> getAllRows() async {
    List<Map<String, dynamic>> maps =  await _colorDao.queryAllRows();
    _colorList.clear();
    maps.forEach((map) => _colorList.add(PetColorTpModel.fromMap(map)));
    return _colorList;
  }

  Future<List<PetColorTpModel>> getRows({int limit = 10, int offset = 0}) async {
    List<Map<String, dynamic>> maps =  await _colorDao.queryRows(limit, offset);
    // print(maps);
    _colorList.clear();
    maps.forEach((map) => _colorList.add(PetColorTpModel.fromMap(map)));
    return _colorList;
  }

  // Retorna a linha que contem o id do model passado. Caso nao encontre
  // retorna null
  Future<PetColorTpModel> getById(int id) async {
    return _colorDao.queryById(PetColorTpModel(idPetColorStatusTp: id)).then((map) => PetColorTpModel.fromMap(map));
  }

  Future<int> delete(int id) async {
    int result = await _colorDao.delete(PetColorTpModel(idPetColorStatusTp: id));
    _colorList.removeWhere((item) => item.idPetColorStatusTp == id);
    return result;
  }
}