

import 'package:flutter/material.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/dao/pet_pet_vs_pet_owner.dart';
import 'package:pet/model/pet_vs_pet_owner_model.dart';

class PetVsPetOwnerController {
  final PetVsPetOwnerDao _petVsPetOwnerDao;
  final List<PetVsPetOwnerModel> _petVsPetOwnerList  = [];
  // final Widget _petVsPetOwnerGui;
  // final EnumSideType _type;

  PetVsPetOwnerController({EnumSideType type = EnumSideType.client, Widget gui}) :
    // _type = type,
    _petVsPetOwnerDao = PetVsPetOwnerDao(type: type);
    // _petVsPetOwnerGui = gui;
  
  Future<List<PetVsPetOwnerModel>> getAddressList () async { 
    return _petVsPetOwnerList.isEmpty? getAllRows() : _petVsPetOwnerList;
  }

  Future<int> insert(PetVsPetOwnerModel obj) async {
    // result é o conteudo da PK da linha inserida.
    int result;
    try {
      result = await _petVsPetOwnerDao.insert(obj);
    } catch(e) {
      return 0;
    }
    _petVsPetOwnerList.add(obj);
    return result;
  }

  Future<int> update(PetVsPetOwnerModel obj) async {
    int result = await _petVsPetOwnerDao.update(obj);
    if (result > 0) {
      _petVsPetOwnerList.add(obj);
    }
    return result;
  }

  Future<List<PetVsPetOwnerModel>> getAllRows() async {
    List<Map<String, dynamic>> maps =  await _petVsPetOwnerDao.queryAllRows();
    _petVsPetOwnerList.clear();
    maps.forEach((map) => _petVsPetOwnerList.add(PetVsPetOwnerModel.fromMap(map)));
    return _petVsPetOwnerList;
  }

  Future<List<PetVsPetOwnerModel>> getRows({int limit = 10, int offset = 0}) async {
    List<Map<String, dynamic>> maps =  await _petVsPetOwnerDao.queryRows(limit, offset);
    // print(maps);
    _petVsPetOwnerList.clear();
    maps.forEach((map) => _petVsPetOwnerList.add(PetVsPetOwnerModel.fromMap(map)));
    return _petVsPetOwnerList;
  }

  // Retorna a linha que contem o id do model passado. Caso nao encontre
  // retorna null
  Future<PetVsPetOwnerModel> getById(int id) async {
    return _petVsPetOwnerDao.queryById(PetVsPetOwnerModel(idPetVsPetOwner: id)).then((map) => PetVsPetOwnerModel.fromMap(map));
  }

  Future<int> delete(int id) async {
    int result = await _petVsPetOwnerDao.delete(PetVsPetOwnerModel(idPetVsPetOwner: id));
    _petVsPetOwnerList.removeWhere((item) => item.idPetVsPetOwner == id);
    return result;
  }
}