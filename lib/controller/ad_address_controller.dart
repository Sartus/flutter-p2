

import 'package:flutter/material.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/dao/ad_address_dao.dart';
import 'package:pet/model/ad_address_model.dart';
import 'package:pet/model/us_user_vs_address_model.dart';

class AddressController {
  final AddressDao _addressDao;
  final List<AddressModel> _addressList  = [];
  // final Widget _addressGui;
  // final EnumSideType _type;

  AddressController({EnumSideType type = EnumSideType.client, Widget gui}) :
    // _type = type,
    _addressDao = AddressDao(type: type);
    // _addressGui = gui;
  
  Future<List<AddressModel>> getAddressList () async { 
    return _addressList.isEmpty? getAllRows() : _addressList;
  }

  Future<int> insert(AddressModel obj) async {
    // result é o conteudo da PK da linha inserida.
    int result;
    try {
      result = await _addressDao.insert(obj);
    } catch(e) {
      return 0;
    }
    _addressList.add(obj);
    return result;
  }

  Future<int> update(AddressModel obj) async {
    int result = await _addressDao.update(obj);
    if (result > 0) {
      _addressList.add(obj);
    }
    return result;
  }

  Future<List<AddressModel>> getAllRows() async {
    List<Map<String, dynamic>> maps =  await _addressDao.queryAllRows();
    _addressList.clear();
    maps.forEach((map) => _addressList.add(AddressModel.fromMap(map)));
    return _addressList;
  }

  Future<List<AddressModel>> getRows({int limit = 10, int offset = 0}) async {
    List<Map<String, dynamic>> maps = await _addressDao.queryRows(limit, offset);
    _addressList.clear();
    maps.forEach((map) => _addressList.add(AddressModel.fromMap(map)));
    return _addressList;
  }

  // Retorna a linha que contem o id do model passado. Caso nao encontre
  // retorna null
  Future<AddressModel> getById(int id) async {
    return _addressDao.queryById(AddressModel(idAddress: id)).then((map) => AddressModel.fromMap(map));
  }

  Future<AddressModel> getByUserId(int id) async { 
    return _addressDao.queryByUserId(UserVsAddressModel(userId: id)).then((map) => AddressModel.fromMap(map));
  }

  Future<int> delete(int id) async {
    int result = await _addressDao.delete(AddressModel(idAddress: id));
    _addressList.removeWhere((item) => item.idAddress == id);
    return result;
  }
}