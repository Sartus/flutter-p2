

import 'package:flutter/material.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/dao/pet_owner_dao.dart';
import 'package:pet/model/pet_owner_model.dart';

class PetOwnerController {
  final PetOwnerDao _ownerDao;
  final List<PetOwnerModel> _ownerList  = [];
  // final Widget _ownerGui;
  // final EnumSideType _type;

  PetOwnerController({EnumSideType type = EnumSideType.client, Widget gui}) :
    // _type = type,
    _ownerDao = PetOwnerDao(type: type);
    // _ownerGui = gui;
  
  Future<List<PetOwnerModel>> getAddressList () async { 
    return _ownerList.isEmpty? getAllRows() : _ownerList;
  }

  Future<int> insert(PetOwnerModel obj) async {
    // result é o conteudo da PK da linha inserida.
    int result;
    try {
      result = await _ownerDao.insert(obj);
    } catch(e) {
      return 0;
    }
    _ownerList.add(obj);
    return result;
  }

  Future<int> update(PetOwnerModel obj) async {
    int result = await _ownerDao.update(obj);
    if (result > 0) {
      _ownerList.add(obj);
    }
    return result;
  }

  Future<List<PetOwnerModel>> getAllRows() async {
    List<Map<String, dynamic>> maps =  await _ownerDao.queryAllRows();
    _ownerList.clear();
    maps.forEach((map) => _ownerList.add(PetOwnerModel.fromMap(map)));
    return _ownerList;
  }

  Future<List<PetOwnerModel>> getRows({int limit = 10, int offset = 0}) async {
    List<Map<String, dynamic>> maps =  await _ownerDao.queryRows(limit, offset);
    _ownerList.clear();
    maps.forEach((map) => _ownerList.add(PetOwnerModel.fromMap(map)));
    return _ownerList;
  }

  // Retorna a linha que contem o id do model passado. Caso nao encontre
  // retorna null
  Future<PetOwnerModel> getById(int id) async {
    return _ownerDao.queryById(PetOwnerModel(idPetOwner: id)).then((map) => PetOwnerModel.fromMap(map));
  }

  // Retorna a linha que contem o id do model passado. Caso nao encontre
  // retorna null
  Future<PetOwnerModel> getByUserId(int id) async {
    return _ownerDao.queryByUserId(PetOwnerModel(userId: id)).then((map) => PetOwnerModel.fromMap(map));
  }

  Future<int> delete(int id) async {
     int result = await _ownerDao.delete(PetOwnerModel(idPetOwner: id));
    _ownerList.removeWhere((item) => item.idPetOwner == id);
    return result;
  }
}