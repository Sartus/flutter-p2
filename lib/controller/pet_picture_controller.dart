

import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/dao/pet_picture_dao.dart';
import 'package:pet/model/pet_picture_model.dart';

import 'package:http/http.dart' as http;


class PetPictureController {
  final PetPictureDao _pictureDao;
  final List<PetPictureModel> _pictureList  = [];
  final List<String> _namePhotos = [];
  // final Widget _pictureGui;
  // final EnumSideType _type;

  PetPictureController({EnumSideType type = EnumSideType.client, Widget gui}) :
    // _type = type,
    _pictureDao = PetPictureDao(type: type);
    // _pictureGui = gui;
  
  Future<List<PetPictureModel>> getAddressList () async { 
    return _pictureList.isEmpty? getAllRows() : _pictureList;
  }

  get namePhotos => _namePhotos;

  Future<int> insert(PetPictureModel obj) async {
    // result é o conteudo da PK da linha inserida.
    int result;
    try {
      result = await _pictureDao.insert(obj);
    } catch(e) {
      return 0;
    }
    _pictureList.add(obj);
    return result;
  }

  Future<int> update(PetPictureModel obj) async {
    int result = await _pictureDao.update(obj);
    if (result > 0) {
      _pictureList.add(obj);
    }
    return result;
  }

  Future<List<PetPictureModel>> getAllRows() async {
    List<Map<String, dynamic>> maps =  await _pictureDao.queryAllRows();
    _pictureList.clear();
    maps.forEach((map) => _pictureList.add(PetPictureModel.fromMap(map)));
    return _pictureList;
  }

  Future<List<PetPictureModel>> getRows({int limit = 10, int offset = 0}) async {
    List<Map<String, dynamic>> maps =  await _pictureDao.queryRows(limit, offset);
    // print(maps);
    _pictureList.clear();
    maps.forEach((map) => _pictureList.add(PetPictureModel.fromMap(map)));
    return _pictureList;
  }

  // Retorna a linha que contem o id do model passado. Caso nao encontre
  // retorna null
  Future<PetPictureModel> getById(int id) async {
    return _pictureDao.queryById(PetPictureModel(idPetPicture: id)).then((map) => PetPictureModel.fromMap(map));
  }

  Future<int> delete(int id) async {
    int result = await _pictureDao.delete(PetPictureModel(idPetPicture: id));
    _pictureList.removeWhere((item) => item.idPetPicture == id);
    return result;
  }

  Future<String> getImage() async {
    String partialUrlPhoto;
    const url = 'https://dog.ceo/api/breeds/image/random';
    final response = await http.get(url);

    if (response.statusCode == 200) {
        Map<String, dynamic> body = jsonDecode(response.body);
        partialUrlPhoto = body["message"].toString().split("/").getRange(4, 6).join("/");
      } else {
        throw Exception('Failed to load post');
      }
    return partialUrlPhoto;
  }

  Future<List<String>> getImagesAndSave({int qtdPthotos = 20}) async {
    const url = 'https://dog.ceo/api/breeds/image/random';

    for(int i = 0; i < qtdPthotos; i++) {
      // Faz a requisicao a api
      final response = await http.get(url);
      // print("\nResposta" + response.body);
      if (response.statusCode == 200) {
        Map<String, dynamic> body = jsonDecode(response.body);
        final image = await http.get(body["message"]);
        String name = await _writePhotoToFileSystem(image.bodyBytes, i + 1);
        _namePhotos.length > i? _namePhotos[i] = name : _namePhotos.add(name);
      } else {
          // If that call was not successful, throw an error.
          throw Exception('Failed to load post');
      }
    }
    return _namePhotos;
  }

  Future<String> _writePhotoToFileSystem(Uint8List body, int picId) async {
    var documentDirectory = await getApplicationDocumentsDirectory();
    var firstPath = documentDirectory.path + "/pic";
    var filePathAndName = documentDirectory.path + "/pic/pet_photo_$picId" + ".jpg";
    // "_$picId" + ".jpg";
    await Directory(firstPath).create(recursive: true);
    File file2 = new File(filePathAndName);
    file2.writeAsBytesSync(body);
    return filePathAndName;
  }
}