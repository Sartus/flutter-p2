

import 'package:flutter/material.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/dao/pet_favorite_play_tp_dao.dart';
import 'package:pet/model/pet_favorite_play_tp_model.dart';

class PetFavoritePlayTpController {
  final PetFavoritePlayTpDao _favoriteDao;
  final List<PetFavoritePlayTpModel> _favoriteList  = [];
  // final Widget _favoriteGui;
  // final EnumSideType _type;

  PetFavoritePlayTpController({EnumSideType type = EnumSideType.client, Widget gui}) :
    // _type = type,
    _favoriteDao = PetFavoritePlayTpDao(type: type);
    // _favoriteGui = gui;
  
  Future<List<PetFavoritePlayTpModel>> getAddressList () async { 
    return _favoriteList.isEmpty? getAllRows() : _favoriteList;
  }

  Future<int> insert(PetFavoritePlayTpModel obj) async {
    // result é o conteudo da PK da linha inserida.
    int result;
    try {
      result = await _favoriteDao.insert(obj);
    } catch(e) {
      return 0;
    }
    _favoriteList.add(obj);
    return result;
  }

  Future<int> update(PetFavoritePlayTpModel obj) async {
    int result = await _favoriteDao.update(obj);
    if (result > 0) {
      _favoriteList.add(obj);
    }
    return result;
  }

  Future<List<PetFavoritePlayTpModel>> getAllRows() async {
    List<Map<String, dynamic>> maps =  await _favoriteDao.queryAllRows();
    _favoriteList.clear();
    maps.forEach((map) => _favoriteList.add(PetFavoritePlayTpModel.fromMap(map)));
    return _favoriteList;
  }

  Future<List<PetFavoritePlayTpModel>> getRows({int limit = 10, int offset = 0}) async {
    List<Map<String, dynamic>> maps =  await _favoriteDao.queryRows(limit, offset);
    // print(maps);
    _favoriteList.clear();
    maps.forEach((map) => _favoriteList.add(PetFavoritePlayTpModel.fromMap(map)));
    return _favoriteList;
  }

  // Retorna a linha que contem o id do model passado. Caso nao encontre
  // retorna null
  Future<PetFavoritePlayTpModel> getById(int id) async {
    return _favoriteDao.queryById(PetFavoritePlayTpModel(idPetFavoritePlayTp: id)).then((map) => PetFavoritePlayTpModel.fromMap(map));
  }

  Future<int> delete(int id) async {
    int result = await _favoriteDao.delete(PetFavoritePlayTpModel(idPetFavoritePlayTp: id));
    _favoriteList.removeWhere((item) => item.idPetFavoritePlayTp == id);
    return result;
  }
}