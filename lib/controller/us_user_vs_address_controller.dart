

import 'package:flutter/material.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/dao/us_user_vs_address_dao.dart';
import 'package:pet/model/us_user_model.dart';
import 'package:pet/model/us_user_vs_address_model.dart';

class UserVsAddressController {
  final UserVsAddressDao _userVsAddressDao;
  final List<UserVsAddressModel> _userVsAddressList  = [];
  // final Widget _userVsAddressGui;
  // final EnumSideType _type;

  UserVsAddressController({EnumSideType type = EnumSideType.client, Widget gui}) :
    // _type = type,
    _userVsAddressDao = UserVsAddressDao(type: type);
    // _userVsAddressGui = gui;
  
  Future<List<UserVsAddressModel>> getAddressList () async { 
    return _userVsAddressList.isEmpty? getAllRows() : _userVsAddressList;
  }

  Future<int> insert(UserVsAddressModel obj) async {
    // result é o conteudo da PK da linha inserida.
    int result;
    try {
      result = await _userVsAddressDao.insert(obj);
    } catch(e) {
      return 0;
    }
    _userVsAddressList.add(obj);
    return result;
  }

  Future<int> update(UserVsAddressModel obj) async {
    int result = await _userVsAddressDao.update(obj);
    if (result > 0) {
      _userVsAddressList.add(obj);
    }
    return result;
  }

  Future<List<UserVsAddressModel>> getAllRows() async {
    List<Map<String, dynamic>> maps =  await _userVsAddressDao.queryAllRows();
    _userVsAddressList.clear();
    maps.forEach((map) => _userVsAddressList.add(UserVsAddressModel.fromMap(map)));
    return _userVsAddressList;
  }

  Future<List<UserVsAddressModel>> getRows({int limit = 10, int offset = 0}) async {
    List<Map<String, dynamic>> maps =  await _userVsAddressDao.queryRows(limit, offset);
    // print(maps);
    _userVsAddressList.clear();
    maps.forEach((map) => _userVsAddressList.add(UserVsAddressModel.fromMap(map)));
    return _userVsAddressList;
  }

  // Retorna a linha que contem o id do model passado. Caso nao encontre
  // retorna null
  Future<UserVsAddressModel> getById(int id) async {
    return _userVsAddressDao.queryById(UserVsAddressModel(idUserVsAddress: id)).then((map) => UserVsAddressModel.fromMap(map));
  }

  Future<int> delete(int id) async {
    int result = await _userVsAddressDao.delete(UserVsAddressModel(idUserVsAddress: id));
    _userVsAddressList.removeWhere((item) => item.idUserVsAddress == id);
    return result;
  }

  Future<List<Map<String, dynamic>>> matchMaker(UserModel user) async { 
    return _userVsAddressDao.matchMaker(user);
  }
}