

import 'package:flutter/material.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/dao/pet_walker_dao.dart';
import 'package:pet/model/pet_walker_model.dart';

class PetWalkerController {
  final PetWalkerDao _walkerDao;
  final List<PetWalkerModel> _walkerList  = [];
  // final Widget _walkerGui;
  // final EnumSideType _type;

  PetWalkerController({EnumSideType type = EnumSideType.client, Widget gui}) :
    // _type = type,
    _walkerDao = PetWalkerDao(type: type);
    // _walkerGui = gui;

  Future<List<PetWalkerModel>> getWalkerList () async { 
    return _walkerList.isEmpty? getAllRows() : _walkerList;
  }

  Future<int> insert(PetWalkerModel obj) async {
    // result é o conteudo da PK da linha inserida.
    int result;
    try {
      result = await _walkerDao.insert(obj);
    } catch(e) {
      return 0;
    }
    _walkerList.add(obj);
    return result;
  }

  Future<int> update(PetWalkerModel obj) async {
    int result = await _walkerDao.update(obj);
    if (result > 0) {
      _walkerList.add(obj);
    }
    return result;
  }

  Future<List<PetWalkerModel>> getAllRows() async {
    List<Map<String, dynamic>> maps =  await _walkerDao.queryAllRows();
    _walkerList.clear();
    maps.forEach((map) => _walkerList.add(PetWalkerModel.fromMap(map)));
    return _walkerList;
  }

  Future<List<PetWalkerModel>> getRows({int limit = 10, int offset = 0}) async {
    List<Map<String, dynamic>> maps =  await _walkerDao.queryRows(limit, offset);
    // print(maps);
    _walkerList.clear();
    maps.forEach((map) => _walkerList.add(PetWalkerModel.fromMap(map)));
    return _walkerList;
  }

  // Retorna a linha que contem o id do model passado. Caso nao encontre
  // retorna null
  Future<PetWalkerModel> getById(int id) async {
    return _walkerDao.queryById(PetWalkerModel(idPetWalker: id)).then((map) => PetWalkerModel.fromMap(map));
  }

  // Retorna a linha que contem o id do model passado. Caso nao encontre
  // retorna null
  Future<PetWalkerModel> getByUserId(int id) async {
    return _walkerDao.queryByUserId(PetWalkerModel(userId: id)).then((map) => PetWalkerModel.fromMap(map));
  }

  Future<Map<String, dynamic>> getByPetWalkerId(int id) async { 
    return await _walkerDao.queryByPetWalkerId(PetWalkerModel(idPetWalker: id));
  }

  Future<List<Map<String, dynamic>>> getPetByWalkerId(int id) async { 
    return await _walkerDao.queryPetByPetWalkerId(PetWalkerModel(idPetWalker: id));
  }

  Future<int> delete(int id) async {
    int result = await _walkerDao.delete(PetWalkerModel(idPetWalker: id));
    _walkerList.removeWhere((item) => item.idPetWalker == id);
    return result;
  }
}