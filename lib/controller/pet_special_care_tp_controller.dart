

import 'package:flutter/material.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/dao/pet_special_care_tp_dao.dart';
import 'package:pet/model/pet_special_care_tp_model.dart';

class PetSpecialCareTpController {
  final PetSpecialCareTpDao _specialDao;
  final List<PetSpecialCareTpModel> _specialList  = [];
  // final Widget _specialGui;
  // final EnumSideType _type;

  PetSpecialCareTpController({EnumSideType type = EnumSideType.client, Widget gui}) :
    // _type = type,
    _specialDao = PetSpecialCareTpDao(type: type);
    // _specialGui = gui;
  
  Future<List<PetSpecialCareTpModel>> getAddressList () async { 
    return _specialList.isEmpty? getAllRows() : _specialList;
  }

  Future<int> insert(PetSpecialCareTpModel obj) async {
    // result é o conteudo da PK da linha inserida.
    int result;
    try {
      result = await _specialDao.insert(obj);
    } catch(e) {
      return 0;
    }
    _specialList.add(obj);
    return result;
  }

  Future<int> update(PetSpecialCareTpModel obj) async {
    int result = await _specialDao.update(obj);
    if (result > 0) {
      _specialList.add(obj);
    }
    return result;
  }

  Future<List<PetSpecialCareTpModel>> getAllRows() async {
    List<Map<String, dynamic>> maps =  await _specialDao.queryAllRows();
    _specialList.clear();
    maps.forEach((map) => _specialList.add(PetSpecialCareTpModel.fromMap(map)));
    return _specialList;
  }

  Future<List<PetSpecialCareTpModel>> getRows({int limit = 10, int offset = 0}) async {
    List<Map<String, dynamic>> maps =  await _specialDao.queryRows(limit, offset);
    // print(maps);
    _specialList.clear();
    maps.forEach((map) => _specialList.add(PetSpecialCareTpModel.fromMap(map)));
    return _specialList;
  }

  // Retorna a linha que contem o id do model passado. Caso nao encontre
  // retorna null
  Future<PetSpecialCareTpModel> getById(int id) async {
    return _specialDao.queryById(PetSpecialCareTpModel(idPetSpecialCareTp: id)).then((map) => PetSpecialCareTpModel.fromMap(map));
  }

  Future<int> delete(int id) async {
    int result = await _specialDao.delete(PetSpecialCareTpModel(idPetSpecialCareTp: id));
    _specialList.removeWhere((item) => item.idPetSpecialCareTp == id);
    return result;
  }
}