

import 'package:flutter/material.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/dao/pet_status_tp_dao.dart';
import 'package:pet/model/pet_status_tp_model.dart';

class PetStatusTpController {
  final PetStatusTpDao _statusDao;
  final List<PetStatusTpModel> _statusList  = [];
  // final Widget _statusGui;
  // final EnumSideType _type;

  PetStatusTpController({EnumSideType type = EnumSideType.client, Widget gui}) :
    // _type = type,
    _statusDao = PetStatusTpDao(type: type);
    // _statusGui = gui;
  
  Future<List<PetStatusTpModel>> getAddressList () async { 
    return _statusList.isEmpty? getAllRows() : _statusList;
  }

  Future<int> insert(PetStatusTpModel obj) async {
    // result é o conteudo da PK da linha inserida.
    int result;
    try {
      result = await _statusDao.insert(obj);
    } catch(e) {
      return 0;
    }
    _statusList.add(obj);
    return result;
  }

  Future<int> update(PetStatusTpModel obj) async {
    int result = await _statusDao.update(obj);
    if (result > 0) {
      _statusList.add(obj);
    }
    return result;
  }

  Future<List<PetStatusTpModel>> getAllRows() async {
    List<Map<String, dynamic>> maps =  await _statusDao.queryAllRows();
    _statusList.clear();
    maps.forEach((map) => _statusList.add(PetStatusTpModel.fromMap(map)));
    return _statusList;
  }

  Future<List<PetStatusTpModel>> getRows({int limit = 10, int offset = 0}) async {
    List<Map<String, dynamic>> maps =  await _statusDao.queryRows(limit, offset);
    // print(maps);
    _statusList.clear();
    maps.forEach((map) => _statusList.add(PetStatusTpModel.fromMap(map)));
    return _statusList;
  }

  // Retorna a linha que contem o id do model passado. Caso nao encontre
  // retorna null
  Future<PetStatusTpModel> getById(int id) async {
    return _statusDao.queryById(PetStatusTpModel(idPetStatusTp: id)).then((map) => PetStatusTpModel.fromMap(map));
  }

  Future<int> delete(int id) async {
    int result = await _statusDao.delete(PetStatusTpModel(idPetStatusTp: id));
    _statusList.removeWhere((item) => item.idPetStatusTp == id);
    return result;
  }
}