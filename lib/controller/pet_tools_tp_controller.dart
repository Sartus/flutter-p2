

import 'package:flutter/material.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/dao/pet_tools_tp_dao.dart';
import 'package:pet/model/pet_tools_tp_model.dart';

class PetToolsTpController {
  final PetToolsTpDao _toolsDao;
  final List<PetToolsTpModel> _toolsList  = [];
  // final Widget _toolsGui;
  // final EnumSideType _type;

  PetToolsTpController({EnumSideType type = EnumSideType.client, Widget gui}) :
    // _type = type,
    _toolsDao = PetToolsTpDao(type: type);
    // _toolsGui = gui;
  
  Future<List<PetToolsTpModel>> getAddressList () async { 
    return _toolsList.isEmpty? getAllRows() : _toolsList;
  }

  Future<int> insert(PetToolsTpModel obj) async {
    // result é o conteudo da PK da linha inserida.
    int result;
    try {
      result = await _toolsDao.insert(obj);
    } catch(e) {
      return 0;
    }
    _toolsList.add(obj);
    return result;
  }

  Future<int> update(PetToolsTpModel obj) async {
    int result = await _toolsDao.update(obj);
    if (result > 0) {
      _toolsList.add(obj);
    }
    return result;
  }

  Future<List<PetToolsTpModel>> getAllRows() async {
    List<Map<String, dynamic>> maps =  await _toolsDao.queryAllRows();
    _toolsList.clear();
    maps.forEach((map) => _toolsList.add(PetToolsTpModel.fromMap(map)));
    return _toolsList;
  }

  Future<List<PetToolsTpModel>> getRows({int limit = 10, int offset = 0}) async {
    List<Map<String, dynamic>> maps =  await _toolsDao.queryRows(limit, offset);
    // print(maps);
    _toolsList.clear();
    maps.forEach((map) => _toolsList.add(PetToolsTpModel.fromMap(map)));
    return _toolsList;
  }

  // Retorna a linha que contem o id do model passado. Caso nao encontre
  // retorna null
  Future<PetToolsTpModel> getById(int id) async {
    return _toolsDao.queryById(PetToolsTpModel(idPetToolsTp: id)).then((map) => PetToolsTpModel.fromMap(map));
  }

  Future<int> delete(int id) async {
    int result = await _toolsDao.delete(PetToolsTpModel(idPetToolsTp: id));
    _toolsList.removeWhere((item) => item.idPetToolsTp == id);
    return result;
  }
}