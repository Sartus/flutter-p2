

import 'dart:convert';
import 'package:http/http.dart' as http;

class FirebaseConection {
  static const _baseURL = 'ENDERECO AQUI';

  // Singleton
  FirebaseConection._privateConstructor();
  static final FirebaseConection instance = FirebaseConection._privateConstructor();

  Future<Map<String, dynamic>> _post(
    Map<String, dynamic> payload,
    String relativeUrl,
    ) async {
    final url = "$_baseURL/$relativeUrl.json";
    final response = await http.post(
      url,
      body: json.encode(payload),
    );
    if(response.statusCode == 200) {
      return json.decode(response.body);
    }
    return null;
  }

  Future<List<Map<String, dynamic>>> _get(String relativeUrl) async {
    final url = "$_baseURL/$relativeUrl.json?";
    final response = await http.get(url);
    if(response.statusCode == 200) {
      return json.decode(response.body);
    }
    return null;
  }

  Future<Map<String, dynamic>> _update(
    String userId,
    Map<String, dynamic> payload,
    String relativeUrl
    ) async {
    final url = "$_baseURL/$relativeUrl/$userId.json";
    final response = await http.post(
      url,
      body: json.encode(payload),
    );
    if(response.statusCode == 200) {
      return json.decode(response.body);
    }
    return null;
  }

  Future _delete(String userId, String relativeUrl) async {
    final url = "$_baseURL/$relativeUrl/$userId.json";
    final response = await http.delete(url);
    if(response.statusCode == 200) {
      return json.decode(response.body);
    }
    return null;
  }

  Future getUserByName(String nome) async {
    return await rawGetBy("Users", "nome", nome);
  }

  Future rawGetBy(String relativeUrl, String collumnName, String equalTo) async {
    final url = '$_baseURL/$relativeUrl.json?orderBy="$collumnName"&equalTo="$equalTo"';
    final response = await http.get(url);
    if(response.statusCode == 200) {
      return json.decode(response.body);
    }
    return null;
  }

  Future<Map<String, dynamic>> cadastraUser(Map<String, dynamic> payload) async {
    return _post(payload, 'Users');
  }

  Future<Map<String, dynamic>> cadastraPetOwner(Map<String, dynamic> payload) async {
    return _post(payload, 'PetOwners');
  }

  Future<Map<String, dynamic>> cadastraPetWalker(Map<String, dynamic> payload) async {
    return _post(payload, 'PetWalkers');
  }

  Future<Map<String, dynamic>> cadastraAddress(Map<String, dynamic> payload) async {
    return _post(payload, 'Address');
  }

  Future<Map<String, dynamic>> cadastraUserVsAddress(Map<String, dynamic> payload) async {
    return _post(payload, 'UserVsAddress');
  }

  Future<Map<String, dynamic>> cadastraPet(Map<String, dynamic> payload) async {
    return _post(payload, 'Pet');
  }

  Future<Map<String, dynamic>> cadastraPetVsPetOwner(Map<String, dynamic> payload) async {
    return _post(payload, 'PetVsPetOwner');
  }
}

FirebaseConection firebaseConection = FirebaseConection.instance;