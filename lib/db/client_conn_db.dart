
import 'dart:io';
import 'package:flutter/services.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class ClientDatabase {
  static const String _databaseName = "Pet_Client_Copia.db";
  static Database _database;

  // torna esta classe singleton
  ClientDatabase._privateConstructor();
  static final ClientDatabase instance = ClientDatabase._privateConstructor();
  Future<void> close() async => await _database.close();

  Future<Database> get database async {
    if(_database != null) // ja existe
      return _database;
    _database = await initDB();
    return _database;
  }

  Future<Database> initDB() async {
    // Gera caminho da copia.
    var databasesPath  = await getDatabasesPath();
    String path = join(databasesPath, _databaseName);

    // Garante que a pasta parente da copia existe
    try {
      await Directory(dirname(path)).create(recursive: true);
    } catch (_) {}

    // Copia o db original
    ByteData data = await rootBundle.load(join("assets/db/", "Pet_Client_Original.db"));
    List<int> bytes = data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
    
    // Escreve e flush os bytes
    await File(path).writeAsBytes(bytes, flush: true);

    return await openDatabase(path, version: 1, onConfigure: _onConfigure);
  }

  // Necessario para permitir o uso de PKs.
  static Future _onConfigure(Database db) async {
    await db.execute('PRAGMA foreign_keys = ON');
  }

  // Delete the database
  Future<void> deleteDb() async {
    if(_database == null) // db nao existe
      return null;
    var databasesPath  = await getDatabasesPath();
    String path = join(databasesPath, _databaseName);
    return deleteDatabase(path);
  }

}