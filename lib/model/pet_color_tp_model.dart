import 'dart:convert';



class PetColorTpModel {
  int idPetColorStatusTp;
  String petColorTpCd;
  String petColorTpAc;
  String petColorTpDs;
  PetColorTpModel({
    this.idPetColorStatusTp,
    this.petColorTpCd,
    this.petColorTpAc,
    this.petColorTpDs,
  });

  PetColorTpModel copyWith({
    int idPetColorStatusTp,
    String petColorTpCd,
    String petColorTpAc,
    String petColorTpDs,
  }) {
    return PetColorTpModel(
      idPetColorStatusTp: idPetColorStatusTp ?? this.idPetColorStatusTp,
      petColorTpCd: petColorTpCd ?? this.petColorTpCd,
      petColorTpAc: petColorTpAc ?? this.petColorTpAc,
      petColorTpDs: petColorTpDs ?? this.petColorTpDs,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'idPetColorStatusTp': idPetColorStatusTp,
      'petColorTpCd': petColorTpCd,
      'petColorTpAc': petColorTpAc,
      'petColorTpDs': petColorTpDs,
    };
  }

  factory PetColorTpModel.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return PetColorTpModel(
      idPetColorStatusTp: map['IdPetColorStatusTp'],
      petColorTpCd: map['PetColorTpCd'],
      petColorTpAc: map['PetColorTpAc'],
      petColorTpDs: map['PetColorTpDs'],
    );
  }

  String toJson() => json.encode(toMap());

  factory PetColorTpModel.fromJson(String source) => PetColorTpModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'PetColorTp(idPetColorStatusTp: $idPetColorStatusTp, petColorTpCd: $petColorTpCd, petColorTpAc: $petColorTpAc, petColorTpDs: $petColorTpDs)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is PetColorTpModel &&
      o.idPetColorStatusTp == idPetColorStatusTp &&
      o.petColorTpCd == petColorTpCd &&
      o.petColorTpAc == petColorTpAc &&
      o.petColorTpDs == petColorTpDs;
  }

  @override
  int get hashCode {
    return idPetColorStatusTp.hashCode ^
      petColorTpCd.hashCode ^
      petColorTpAc.hashCode ^
      petColorTpDs.hashCode;
  }
}
