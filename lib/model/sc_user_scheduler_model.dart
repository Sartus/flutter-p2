
import 'dart:convert';

class UserSchedulerModel {
  int idUserSchedule;
  int userId;
  DateTime beginTs;
  DateTime endTs;
  String title;
  UserSchedulerModel({
    this.idUserSchedule,
    this.userId,
    this.beginTs,
    this.endTs,
    this.title,
  });
 

  UserSchedulerModel copyWith({
    int idUserSchedule,
    int userId,
    DateTime beginTs,
    DateTime endTs,
    String title,
  }) {
    return UserSchedulerModel(
      idUserSchedule: idUserSchedule ?? this.idUserSchedule,
      userId: userId ?? this.userId,
      beginTs: beginTs ?? this.beginTs,
      endTs: endTs ?? this.endTs,
      title: title ?? this.title,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'idUserSchedule': idUserSchedule,
      'userId': userId,
      'beginTs': beginTs==null ? null:beginTs.millisecondsSinceEpoch,
      'endTs': endTs==null ? null:endTs.millisecondsSinceEpoch,
      'title': title,
    };
  }

  factory UserSchedulerModel.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return UserSchedulerModel(
      idUserSchedule: map['IdUserSchedule'],
      userId: map['UserId'],
      // beginTs: DateTime.fromMillisecondsSinceEpoch(map['BeginTs']),
      // endTs: DateTime.fromMillisecondsSinceEpoch(map['EndTs']),
      beginTs: (map['BeginTs']==null)? null: DateTime.fromMillisecondsSinceEpoch(map['BeginTs']),
      endTs: (map['EndTs']==null)? null: DateTime.fromMillisecondsSinceEpoch(map['EndTs']),
      title: map['Title'],
    );
  }

  String toJson() => json.encode(toMap());

  factory UserSchedulerModel.fromJson(String source) => UserSchedulerModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'ClientUserSchedule(idUserSchedule: $idUserSchedule, userId: $userId, beginTs: $beginTs, endTs: $endTs, title: $title)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is UserSchedulerModel &&
      o.idUserSchedule == idUserSchedule &&
      o.userId == userId &&
      o.beginTs == beginTs &&
      o.endTs == endTs &&
      o.title == title;
  }

  @override
  int get hashCode {
    return idUserSchedule.hashCode ^
      userId.hashCode ^
      beginTs.hashCode ^
      endTs.hashCode ^
      title.hashCode;
  }
}
