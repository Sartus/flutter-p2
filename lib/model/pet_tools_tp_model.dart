import 'dart:convert';


class PetToolsTpModel {
  int idPetToolsTp;
  String petToolsTpCd;
  String petToolsTpAc;
  String petToolsTpDs;
  PetToolsTpModel({
    this.idPetToolsTp,
    this.petToolsTpCd,
    this.petToolsTpAc,
    this.petToolsTpDs,
  });

  PetToolsTpModel copyWith({
    int idPetToolsTp,
    String petToolsTpCd,
    String petToolsTpAc,
    String petToolsTpDs,
  }) {
    return PetToolsTpModel(
      idPetToolsTp: idPetToolsTp ?? this.idPetToolsTp,
      petToolsTpCd: petToolsTpCd ?? this.petToolsTpCd,
      petToolsTpAc: petToolsTpAc ?? this.petToolsTpAc,
      petToolsTpDs: petToolsTpDs ?? this.petToolsTpDs,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'idPetToolsTp': idPetToolsTp,
      'petToolsTpCd': petToolsTpCd,
      'petToolsTpAc': petToolsTpAc,
      'petToolsTpDs': petToolsTpDs,
    };
  }

  factory PetToolsTpModel.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return PetToolsTpModel(
      idPetToolsTp: map['IdPetToolsTp'],
      petToolsTpCd: map['PetToolsTpCd'],
      petToolsTpAc: map['PetToolsTpAc'],
      petToolsTpDs: map['PetToolsTpDs'],
    );
  }

  String toJson() => json.encode(toMap());

  factory PetToolsTpModel.fromJson(String source) => PetToolsTpModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'PetToolsTp(idPetToolsTp: $idPetToolsTp, petToolsTpCd: $petToolsTpCd, petToolsTpAc: $petToolsTpAc, petToolsTpDs: $petToolsTpDs)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is PetToolsTpModel &&
      o.idPetToolsTp == idPetToolsTp &&
      o.petToolsTpCd == petToolsTpCd &&
      o.petToolsTpAc == petToolsTpAc &&
      o.petToolsTpDs == petToolsTpDs;
  }

  @override
  int get hashCode {
    return idPetToolsTp.hashCode ^
      petToolsTpCd.hashCode ^
      petToolsTpAc.hashCode ^
      petToolsTpDs.hashCode;
  }
}
