import 'dart:convert';



class PetSizeTpModel {
  int idPetSizeTp;
  String petSizeTpCd;
  String petSizeTpAc;
  String petSizeTpDs;
  PetSizeTpModel({
    this.idPetSizeTp,
    this.petSizeTpCd,
    this.petSizeTpAc,
    this.petSizeTpDs,
  });


  PetSizeTpModel copyWith({
    int idPetSizeTp,
    String petSizeTpCd,
    String petSizeTpAc,
    String petSizeTpDs,
  }) {
    return PetSizeTpModel(
      idPetSizeTp: idPetSizeTp ?? this.idPetSizeTp,
      petSizeTpCd: petSizeTpCd ?? this.petSizeTpCd,
      petSizeTpAc: petSizeTpAc ?? this.petSizeTpAc,
      petSizeTpDs: petSizeTpDs ?? this.petSizeTpDs,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'idPetSizeTp': idPetSizeTp,
      'petSizeTpCd': petSizeTpCd,
      'petSizeTpAc': petSizeTpAc,
      'petSizeTpDs': petSizeTpDs,
    };
  }

  factory PetSizeTpModel.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return PetSizeTpModel(
      idPetSizeTp: map['IdPetSizeTp'],
      petSizeTpCd: map['PetSizeTpCd'],
      petSizeTpAc: map['PetSizeTpAc'],
      petSizeTpDs: map['PetSizeTpDs'],
    );
  }

  String toJson() => json.encode(toMap());

  factory PetSizeTpModel.fromJson(String source) => PetSizeTpModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'PetSizeTp(idPetSizeTp: $idPetSizeTp, petSizeTpCd: $petSizeTpCd, petSizeTpAc: $petSizeTpAc, petSizeTpDs: $petSizeTpDs)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is PetSizeTpModel &&
      o.idPetSizeTp == idPetSizeTp &&
      o.petSizeTpCd == petSizeTpCd &&
      o.petSizeTpAc == petSizeTpAc &&
      o.petSizeTpDs == petSizeTpDs;
  }

  @override
  int get hashCode {
    return idPetSizeTp.hashCode ^
      petSizeTpCd.hashCode ^
      petSizeTpAc.hashCode ^
      petSizeTpDs.hashCode;
  }
}
