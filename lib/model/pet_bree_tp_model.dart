

import 'dart:convert';

class PetBreedTpModel {
  int idPetBreedTp;
  String petBreedTpCd;
  String petBreedTpAc;
  String petBreedTpDs;
  int ownerId;
  PetBreedTpModel({
    this.idPetBreedTp,
    this.petBreedTpCd,
    this.petBreedTpAc,
    this.petBreedTpDs,
    this.ownerId,
  });
  
  PetBreedTpModel copyWith({
    int idPetBreedTp,
    String petBreedTpCd,
    String petBreedTpAc,
    String petBreedTpDs,
    int ownerId,
  }) {
    return PetBreedTpModel(
      idPetBreedTp: idPetBreedTp ?? this.idPetBreedTp,
      petBreedTpCd: petBreedTpCd ?? this.petBreedTpCd,
      petBreedTpAc: petBreedTpAc ?? this.petBreedTpAc,
      petBreedTpDs: petBreedTpDs ?? this.petBreedTpDs,
      ownerId: ownerId ?? this.ownerId,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'idPetBreedTp': idPetBreedTp,
      'petBreedTpCd': petBreedTpCd,
      'petBreedTpAc': petBreedTpAc,
      'petBreedTpDs': petBreedTpDs,
      'ownerId': ownerId,
    };
  }

  factory PetBreedTpModel.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return PetBreedTpModel(
      idPetBreedTp: map['IdPetBreedTp'],
      petBreedTpCd: map['PetBreedTpCd'],
      petBreedTpAc: map['PetBreedTpAc'],
      petBreedTpDs: map['PetBreedTpDs'],
      ownerId: map['OwnerId'],
    );
  }

  String toJson() => json.encode(toMap());

  factory PetBreedTpModel.fromJson(String source) => PetBreedTpModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'PetBreedTp(idPetBreedTp: $idPetBreedTp, petBreedTpCd: $petBreedTpCd, petBreedTpAc: $petBreedTpAc, petBreedTpDs: $petBreedTpDs, ownerId: $ownerId)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is PetBreedTpModel &&
      o.idPetBreedTp == idPetBreedTp &&
      o.petBreedTpCd == petBreedTpCd &&
      o.petBreedTpAc == petBreedTpAc &&
      o.petBreedTpDs == petBreedTpDs &&
      o.ownerId == ownerId;
  }

  @override
  int get hashCode {
    return idPetBreedTp.hashCode ^
      petBreedTpCd.hashCode ^
      petBreedTpAc.hashCode ^
      petBreedTpDs.hashCode ^
      ownerId.hashCode;
  }
}
