import 'dart:convert';


class PetStatusTpModel {
  int idPetStatusTp;
  String petStatusTpCd;
  String petStatusTpAc;
  String petStatusTpDs;
  PetStatusTpModel({
    this.idPetStatusTp,
    this.petStatusTpCd,
    this.petStatusTpAc,
    this.petStatusTpDs,
  });

  PetStatusTpModel copyWith({
    int idPetStatusTp,
    String petStatusTpCd,
    String petStatusTpAc,
    String petStatusTpDs,
  }) {
    return PetStatusTpModel(
      idPetStatusTp: idPetStatusTp ?? this.idPetStatusTp,
      petStatusTpCd: petStatusTpCd ?? this.petStatusTpCd,
      petStatusTpAc: petStatusTpAc ?? this.petStatusTpAc,
      petStatusTpDs: petStatusTpDs ?? this.petStatusTpDs,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'idPetStatusTp': idPetStatusTp,
      'petStatusTpCd': petStatusTpCd,
      'petStatusTpAc': petStatusTpAc,
      'petStatusTpDs': petStatusTpDs,
    };
  }

  factory PetStatusTpModel.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return PetStatusTpModel(
      idPetStatusTp: map['IdPetStatusTp'],
      petStatusTpCd: map['PetStatusTpCd'],
      petStatusTpAc: map['PetStatusTpAc'],
      petStatusTpDs: map['PetStatusTpDs'],
    );
  }

  String toJson() => json.encode(toMap());

  factory PetStatusTpModel.fromJson(String source) => PetStatusTpModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'PetStatusTp(idPetStatusTp: $idPetStatusTp, petStatusTpCd: $petStatusTpCd, petStatusTpAc: $petStatusTpAc, petStatusTpDs: $petStatusTpDs)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is PetStatusTpModel &&
      o.idPetStatusTp == idPetStatusTp &&
      o.petStatusTpCd == petStatusTpCd &&
      o.petStatusTpAc == petStatusTpAc &&
      o.petStatusTpDs == petStatusTpDs;
  }

  @override
  int get hashCode {
    return idPetStatusTp.hashCode ^
      petStatusTpCd.hashCode ^
      petStatusTpAc.hashCode ^
      petStatusTpDs.hashCode;
  }
}
