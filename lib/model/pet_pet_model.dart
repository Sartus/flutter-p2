

import 'dart:convert';

class PetModel {
  int idPet;
  int petStatusTpId;
  String petNm;
  String relativePathNamePicture;
  PetModel({
    this.idPet,
    this.petStatusTpId,
    this.petNm,
    this.relativePathNamePicture,
  });



  PetModel copyWith({
    int idPet,
    int petStatusTpId,
    String petNm,
    String relativePathNamePicture,
  }) {
    return PetModel(
      idPet: idPet ?? this.idPet,
      petStatusTpId: petStatusTpId ?? this.petStatusTpId,
      petNm: petNm ?? this.petNm,
      relativePathNamePicture: relativePathNamePicture ?? this.relativePathNamePicture,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'idPet': idPet,
      'petStatusTpId': petStatusTpId,
      'petNm': petNm,
      'relativePathNamePicture': relativePathNamePicture,
    };
  }

  factory PetModel.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return PetModel(
      idPet: map['IdPet'],
      petStatusTpId: map['PetStatusTpId'],
      petNm: map['PetNm'],
      relativePathNamePicture: map['RelativePathNamePicture'],
    );
  }

  String toJson() => json.encode(toMap());

  factory PetModel.fromJson(String source) => PetModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Pet(idPet: $idPet, petStatusTpId: $petStatusTpId, petNm: $petNm, relativePathNamePicture: $relativePathNamePicture)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is PetModel &&
      o.idPet == idPet &&
      o.petStatusTpId == petStatusTpId &&
      o.petNm == petNm &&
      o.relativePathNamePicture == relativePathNamePicture;
  }

  @override
  int get hashCode {
    return idPet.hashCode ^
      petStatusTpId.hashCode ^
      petNm.hashCode ^
      relativePathNamePicture.hashCode;
  }
}
