

import 'dart:convert';

class PetPictureModel {
  int idPetPicture;
  int petId;
  int pictureSq;
  String pictureRelativePath;
  String noteDs;
  PetPictureModel({
    this.idPetPicture,
    this.petId,
    this.pictureSq,
    this.pictureRelativePath,
    this.noteDs,
  });


  PetPictureModel copyWith({
    int idPetPicture,
    int petId,
    int pictureSq,
    String pictureRelativePath,
    String noteDs,
  }) {
    return PetPictureModel(
      idPetPicture: idPetPicture ?? this.idPetPicture,
      petId: petId ?? this.petId,
      pictureSq: pictureSq ?? this.pictureSq,
      pictureRelativePath: pictureRelativePath ?? this.pictureRelativePath,
      noteDs: noteDs ?? this.noteDs,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'idPetPicture': idPetPicture,
      'petId': petId,
      'pictureSq': pictureSq,
      'pictureRelativePath': pictureRelativePath,
      'noteDs': noteDs,
    };
  }

  factory PetPictureModel.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return PetPictureModel(
      idPetPicture: map['IdPetPicture'],
      petId: map['PetId'],
      pictureSq: map['PictureSq'],
      pictureRelativePath: map['PictureRelativePath'],
      noteDs: map['NoteDs'],
    );
  }

  String toJson() => json.encode(toMap());

  factory PetPictureModel.fromJson(String source) => PetPictureModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'PetPicture(idPetPicture: $idPetPicture, petId: $petId, pictureSq: $pictureSq, pictureRelativePath: $pictureRelativePath, noteDs: $noteDs)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is PetPictureModel &&
      o.idPetPicture == idPetPicture &&
      o.petId == petId &&
      o.pictureSq == pictureSq &&
      o.pictureRelativePath == pictureRelativePath &&
      o.noteDs == noteDs;
  }

  @override
  int get hashCode {
    return idPetPicture.hashCode ^
      petId.hashCode ^
      pictureSq.hashCode ^
      pictureRelativePath.hashCode ^
      noteDs.hashCode;
  }
}
