

import 'dart:convert';

class PetWalkerModel {
  int idPetWalker;
  int userId;
  int petStatusTpId;
  int rateNr;
  PetWalkerModel({
    this.idPetWalker,
    this.userId,
    this.petStatusTpId,
    this.rateNr,
  });

  PetWalkerModel copyWith({
    int idPetWalker,
    int userId,
    int petStatusTpId,
    int rateNr,
  }) {
    return PetWalkerModel(
      idPetWalker: idPetWalker ?? this.idPetWalker,
      userId: userId ?? this.userId,
      petStatusTpId: petStatusTpId ?? this.petStatusTpId,
      rateNr: rateNr ?? this.rateNr,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'idPetWalker': idPetWalker,
      'userId': userId,
      'petStatusTpId': petStatusTpId,
      'rateNr': rateNr,
    };
  }

  factory PetWalkerModel.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return PetWalkerModel(
      idPetWalker: map['IdPetWalker'],
      userId: map['UserId'],
      petStatusTpId: map['PetStatusTpId'],
      rateNr: map['RateNr'],
    );
  }

  String toJson() => json.encode(toMap());

  factory PetWalkerModel.fromJson(String source) => PetWalkerModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'PetWalkerModel(idPetWalker: $idPetWalker, userId: $userId, petStatusTpId: $petStatusTpId, rateNr: $rateNr)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is PetWalkerModel &&
      o.idPetWalker == idPetWalker &&
      o.userId == userId &&
      o.petStatusTpId == petStatusTpId &&
      o.rateNr == rateNr;
  }

  @override
  int get hashCode {
    return idPetWalker.hashCode ^
      userId.hashCode ^
      petStatusTpId.hashCode ^
      rateNr.hashCode;
  }
}
