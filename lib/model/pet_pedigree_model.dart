

import 'dart:convert';

class PetPedigreeModel {
  int idPetPedigree;
  int petId;
  int petBreedTpId;
  String pedigreeNr;
  String pictureRelativePath;
  PetPedigreeModel({
    this.idPetPedigree,
    this.petId,
    this.petBreedTpId,
    this.pedigreeNr,
    this.pictureRelativePath,
  });

  PetPedigreeModel copyWith({
    int idPetPedigree,
    int petId,
    int petBreedTpId,
    String pedigreeNr,
    String pictureRelativePath,
  }) {
    return PetPedigreeModel(
      idPetPedigree: idPetPedigree ?? this.idPetPedigree,
      petId: petId ?? this.petId,
      petBreedTpId: petBreedTpId ?? this.petBreedTpId,
      pedigreeNr: pedigreeNr ?? this.pedigreeNr,
      pictureRelativePath: pictureRelativePath ?? this.pictureRelativePath,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'idPetPedigree': idPetPedigree,
      'petId': petId,
      'petBreedTpId': petBreedTpId,
      'pedigreeNr': pedigreeNr,
      'pictureRelativePath': pictureRelativePath,
    };
  }

  factory PetPedigreeModel.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return PetPedigreeModel(
      idPetPedigree: map['IdPetPedigree'],
      petId: map['PetId'],
      petBreedTpId: map['PetBreedTpId'],
      pedigreeNr: map['PedigreeNr'],
      pictureRelativePath: map['PictureRelativePath'],
    );
  }

  String toJson() => json.encode(toMap());

  factory PetPedigreeModel.fromJson(String source) => PetPedigreeModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'PetPedigree(idPetPedigree: $idPetPedigree, petId: $petId, petBreedTpId: $petBreedTpId, pedigreeNr: $pedigreeNr, pictureRelativePath: $pictureRelativePath)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is PetPedigreeModel &&
      o.idPetPedigree == idPetPedigree &&
      o.petId == petId &&
      o.petBreedTpId == petBreedTpId &&
      o.pedigreeNr == pedigreeNr &&
      o.pictureRelativePath == pictureRelativePath;
  }

  @override
  int get hashCode {
    return idPetPedigree.hashCode ^
      petId.hashCode ^
      petBreedTpId.hashCode ^
      pedigreeNr.hashCode ^
      pictureRelativePath.hashCode;
  }
}
