

import 'dart:convert';

class UserVsAddressModel {
  int idUserVsAddress;
  int userId;
  int addressId;
  String number;
  String complementary;
  UserVsAddressModel({
    this.idUserVsAddress,
    this.userId,
    this.addressId,
    this.number,
    this.complementary,
  });

  UserVsAddressModel copyWith({
    int idUserVsAddress,
    int userId,
    int addressId,
    String number,
    String complementary,
  }) {
    return UserVsAddressModel(
      idUserVsAddress: idUserVsAddress ?? this.idUserVsAddress,
      userId: userId ?? this.userId,
      addressId: addressId ?? this.addressId,
      number: number ?? this.number,
      complementary: complementary ?? this.complementary,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'idUserVsAddress': idUserVsAddress,
      'userId': userId,
      'addressId': addressId,
      'number': number,
      'complementary': complementary,
    };
  }

  factory UserVsAddressModel.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return UserVsAddressModel(
      idUserVsAddress: map['IdUserVsAddress'],
      userId: map['UserId'],
      addressId: map['AddressId'],
      number: map['Number'],
      complementary: map['Complementary'],
    );
  }

  String toJson() => json.encode(toMap());

  factory UserVsAddressModel.fromJson(String source) => UserVsAddressModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'UserVsAddress(idUserVsAddress: $idUserVsAddress, userId: $userId, addressId: $addressId, number: $number, complementary: $complementary)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is UserVsAddressModel &&
      o.idUserVsAddress == idUserVsAddress &&
      o.userId == userId &&
      o.addressId == addressId &&
      o.number == number &&
      o.complementary == complementary;
  }

  @override
  int get hashCode {
    return idUserVsAddress.hashCode ^
      userId.hashCode ^
      addressId.hashCode ^
      number.hashCode ^
      complementary.hashCode;
  }
}
