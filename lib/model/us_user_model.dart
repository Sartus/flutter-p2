

import 'dart:convert';

class UserModel {
  int idUser;
  String userName;
  String userPwd;
  int petStatusTpId;
  String relativePathNamePicture;
  UserModel({
    this.idUser,
    this.userName,
    this.userPwd,
    this.petStatusTpId,
    this.relativePathNamePicture,
  });

  UserModel copyWith({
    int idUser,
    String userName,
    String userPwd,
    int petStatusTpId,
    String relativePathNamePicture,
  }) {
    return UserModel(
      idUser: idUser ?? this.idUser,
      userName: userName ?? this.userName,
      userPwd: userPwd ?? this.userPwd,
      petStatusTpId: petStatusTpId ?? this.petStatusTpId,
      relativePathNamePicture: relativePathNamePicture ?? this.relativePathNamePicture,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'idUser': idUser,
      'userName': userName,
      'userPwd': userPwd,
      'petStatusTpId': petStatusTpId,
      'relativePathNamePicture': relativePathNamePicture,
    };
  }

  factory UserModel.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return UserModel(
      idUser: map['IdUser'],
      userName: map['UserName'],
      userPwd: map['UserPwd'],
      petStatusTpId: map['PetStatusTpId'],
      relativePathNamePicture: map['RelativePathNamePicture'],
    );
  }

  String toJson() => json.encode(toMap());

  factory UserModel.fromJson(String source) => UserModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'User(idUser: $idUser, userName: $userName, userPwd: $userPwd, petStatusTpId: $petStatusTpId, relativePathNamePicture: $relativePathNamePicture)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is UserModel &&
      o.idUser == idUser &&
      o.userName == userName &&
      o.userPwd == userPwd &&
      o.petStatusTpId == petStatusTpId &&
      o.relativePathNamePicture == relativePathNamePicture;
  }

  @override
  int get hashCode {
    return idUser.hashCode ^
      userName.hashCode ^
      userPwd.hashCode ^
      petStatusTpId.hashCode ^
      relativePathNamePicture.hashCode;
  }
}
