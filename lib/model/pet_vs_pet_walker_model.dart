

import 'dart:convert';

class PetVsPetWalkerModel {
  int idPetVsPetWalker;
  int petId;
  int petWalkerId;
  int petStatusTpId;
  int isFavority;
  PetVsPetWalkerModel({
    this.idPetVsPetWalker,
    this.petId,
    this.petWalkerId,
    this.petStatusTpId,
    this.isFavority,
  });


  PetVsPetWalkerModel copyWith({
    int idPetVsPetWalker,
    int petId,
    int petWalkerId,
    int petStatusTpId,
    int isFavority,
  }) {
    return PetVsPetWalkerModel(
      idPetVsPetWalker: idPetVsPetWalker ?? this.idPetVsPetWalker,
      petId: petId ?? this.petId,
      petWalkerId: petWalkerId ?? this.petWalkerId,
      petStatusTpId: petStatusTpId ?? this.petStatusTpId,
      isFavority: isFavority ?? this.isFavority,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'idPetVsPetWalker': idPetVsPetWalker,
      'petId': petId,
      'petWalkerId': petWalkerId,
      'petStatusTpId': petStatusTpId,
      'isFavority': isFavority,
    };
  }

  factory PetVsPetWalkerModel.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return PetVsPetWalkerModel(
      idPetVsPetWalker: map['IdPetVsPetWalker'],
      petId: map['PetId'],
      petWalkerId: map['PetWalkerId'],
      petStatusTpId: map['PetStatusTpId'],
      isFavority: map['IsFavority'],
    );
  }

  String toJson() => json.encode(toMap());

  factory PetVsPetWalkerModel.fromJson(String source) => PetVsPetWalkerModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'PetVsPetWalkerModel(idPetVsPetWalker: $idPetVsPetWalker, petId: $petId, petWalkerId: $petWalkerId, petStatusTpId: $petStatusTpId, isFavority: $isFavority)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is PetVsPetWalkerModel &&
      o.idPetVsPetWalker == idPetVsPetWalker &&
      o.petId == petId &&
      o.petWalkerId == petWalkerId &&
      o.petStatusTpId == petStatusTpId &&
      o.isFavority == isFavority;
  }

  @override
  int get hashCode {
    return idPetVsPetWalker.hashCode ^
      petId.hashCode ^
      petWalkerId.hashCode ^
      petStatusTpId.hashCode ^
      isFavority.hashCode;
  }
}
