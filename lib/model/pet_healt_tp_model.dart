import 'dart:convert';


class PetHealthTpModel {
  int idPetHealthTp;
  String petHealthTpCd;
  String petHealthTpAc;
  String petHealthTpDs;
  PetHealthTpModel({
    this.idPetHealthTp,
    this.petHealthTpCd,
    this.petHealthTpAc,
    this.petHealthTpDs,
  });

  PetHealthTpModel copyWith({
    int idPetHealthTp,
    String petHealthTpCd,
    String petHealthTpAc,
    String petHealthTpDs,
  }) {
    return PetHealthTpModel(
      idPetHealthTp: idPetHealthTp ?? this.idPetHealthTp,
      petHealthTpCd: petHealthTpCd ?? this.petHealthTpCd,
      petHealthTpAc: petHealthTpAc ?? this.petHealthTpAc,
      petHealthTpDs: petHealthTpDs ?? this.petHealthTpDs,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'idPetHealthTp': idPetHealthTp,
      'petHealthTpCd': petHealthTpCd,
      'petHealthTpAc': petHealthTpAc,
      'petHealthTpDs': petHealthTpDs,
    };
  }

  factory PetHealthTpModel.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return PetHealthTpModel(
      idPetHealthTp: map['IdPetHealthTp'],
      petHealthTpCd: map['PetHealthTpCd'],
      petHealthTpAc: map['PetHealthTpAc'],
      petHealthTpDs: map['PetHealthTpDs'],
    );
  }

  String toJson() => json.encode(toMap());

  factory PetHealthTpModel.fromJson(String source) => PetHealthTpModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'PetHealthTp(idPetHealthTp: $idPetHealthTp, petHealthTpCd: $petHealthTpCd, petHealthTpAc: $petHealthTpAc, petHealthTpDs: $petHealthTpDs)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is PetHealthTpModel &&
      o.idPetHealthTp == idPetHealthTp &&
      o.petHealthTpCd == petHealthTpCd &&
      o.petHealthTpAc == petHealthTpAc &&
      o.petHealthTpDs == petHealthTpDs;
  }

  @override
  int get hashCode {
    return idPetHealthTp.hashCode ^
      petHealthTpCd.hashCode ^
      petHealthTpAc.hashCode ^
      petHealthTpDs.hashCode;
  }
}
