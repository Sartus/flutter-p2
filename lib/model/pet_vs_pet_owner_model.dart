

import 'dart:convert';

class PetVsPetOwnerModel {
  int idPetVsPetOwner;
  int petId;
  int petOwnerId;
  int petStatusTpId;
  PetVsPetOwnerModel({
    this.idPetVsPetOwner,
    this.petId,
    this.petOwnerId,
    this.petStatusTpId,
  });

  PetVsPetOwnerModel copyWith({
    int idPetVsPetOwner,
    int petId,
    int petOwnerId,
    int petStatusTpId,
  }) {
    return PetVsPetOwnerModel(
      idPetVsPetOwner: idPetVsPetOwner ?? this.idPetVsPetOwner,
      petId: petId ?? this.petId,
      petOwnerId: petOwnerId ?? this.petOwnerId,
      petStatusTpId: petStatusTpId ?? this.petStatusTpId,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'idPetVsPetOwner': idPetVsPetOwner,
      'petId': petId,
      'petOwnerId': petOwnerId,
      'petStatusTpId': petStatusTpId,
    };
  }

  factory PetVsPetOwnerModel.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return PetVsPetOwnerModel(
      idPetVsPetOwner: map['IdPetVsPetOwner'],
      petId: map['PetId'],
      petOwnerId: map['PetOwnerId'],
      petStatusTpId: map['PetStatusTpId'],
    );
  }

  String toJson() => json.encode(toMap());

  factory PetVsPetOwnerModel.fromJson(String source) => PetVsPetOwnerModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'PetVsPetOwner(idPetVsPetOwner: $idPetVsPetOwner, petId: $petId, petOwnerId: $petOwnerId, petStatusTpId: $petStatusTpId)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is PetVsPetOwnerModel &&
      o.idPetVsPetOwner == idPetVsPetOwner &&
      o.petId == petId &&
      o.petOwnerId == petOwnerId &&
      o.petStatusTpId == petStatusTpId;
  }

  @override
  int get hashCode {
    return idPetVsPetOwner.hashCode ^
      petId.hashCode ^
      petOwnerId.hashCode ^
      petStatusTpId.hashCode;
  }
}
