// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en_US locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en_US';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "addressCityText" : MessageLookupByLibrary.simpleMessage("City"),
    "addressComplementaryText" : MessageLookupByLibrary.simpleMessage("Complementary"),
    "addressNeighborhoodText" : MessageLookupByLibrary.simpleMessage("Neighborhood"),
    "addressNumberText" : MessageLookupByLibrary.simpleMessage("Number"),
    "addressStateText" : MessageLookupByLibrary.simpleMessage("State"),
    "addressText" : MessageLookupByLibrary.simpleMessage("Address"),
    "addressTitleText" : MessageLookupByLibrary.simpleMessage("User\'s Address"),
    "buttonCancelText" : MessageLookupByLibrary.simpleMessage("Cancel"),
    "buttonCreateText" : MessageLookupByLibrary.simpleMessage("Create"),
    "buttonNavigationBarAddressText" : MessageLookupByLibrary.simpleMessage("Address"),
    "buttonNavigationBarHomeText" : MessageLookupByLibrary.simpleMessage("Home"),
    "buttonNavigationBarPetText" : MessageLookupByLibrary.simpleMessage("Pet"),
    "buttonNavigationBarProfileText" : MessageLookupByLibrary.simpleMessage("Profile"),
    "buttonNavigationBarSettingText" : MessageLookupByLibrary.simpleMessage("Setting"),
    "invalidDialog1Text" : MessageLookupByLibrary.simpleMessage("There are empty fields."),
    "invalidDialog2Text" : MessageLookupByLibrary.simpleMessage("Error during pet creation."),
    "invalidDialog3Text" : MessageLookupByLibrary.simpleMessage("Error while storing the address."),
    "invalidDialog4Text" : MessageLookupByLibrary.simpleMessage("Invalid user."),
    "invalidDialog5Text" : MessageLookupByLibrary.simpleMessage("The passwords entered are not the same."),
    "invalidDialog6Text" : MessageLookupByLibrary.simpleMessage("The user already exists.\nTry with a different Name."),
    "loginEnterTextButton" : MessageLookupByLibrary.simpleMessage("Enter"),
    "loginForgotPasswordText" : MessageLookupByLibrary.simpleMessage("Forgot Password"),
    "loginInvalidDialogText" : MessageLookupByLibrary.simpleMessage("Name or password incorrect."),
    "loginRegisterText" : MessageLookupByLibrary.simpleMessage("Register"),
    "loginText" : MessageLookupByLibrary.simpleMessage("Login"),
    "nameText" : MessageLookupByLibrary.simpleMessage("Name"),
    "passwordConfirmText" : MessageLookupByLibrary.simpleMessage("Confirm the Password"),
    "passwordText" : MessageLookupByLibrary.simpleMessage("Password"),
    "petGalleryText" : MessageLookupByLibrary.simpleMessage("Gallery"),
    "petPetNameText" : MessageLookupByLibrary.simpleMessage("Pet Name"),
    "petPetRegistrationText" : MessageLookupByLibrary.simpleMessage("Pet Registration"),
    "rateText" : MessageLookupByLibrary.simpleMessage("Rate"),
    "userNameText" : MessageLookupByLibrary.simpleMessage("User Name"),
    "userTitleText" : MessageLookupByLibrary.simpleMessage("User Registration")
  };
}
