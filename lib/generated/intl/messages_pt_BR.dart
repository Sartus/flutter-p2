// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a pt_BR locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'pt_BR';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "addressCityText" : MessageLookupByLibrary.simpleMessage("Cidade"),
    "addressComplementaryText" : MessageLookupByLibrary.simpleMessage("Complementary"),
    "addressNeighborhoodText" : MessageLookupByLibrary.simpleMessage("Bairro"),
    "addressNumberText" : MessageLookupByLibrary.simpleMessage("Numero"),
    "addressStateText" : MessageLookupByLibrary.simpleMessage("Estado"),
    "addressText" : MessageLookupByLibrary.simpleMessage("Endereço"),
    "addressTitleText" : MessageLookupByLibrary.simpleMessage("Endereço do Usuário"),
    "buttonCancelText" : MessageLookupByLibrary.simpleMessage("Cancelar"),
    "buttonCreateText" : MessageLookupByLibrary.simpleMessage("Criar"),
    "buttonNavigationBarAddressText" : MessageLookupByLibrary.simpleMessage("Endereço"),
    "buttonNavigationBarHomeText" : MessageLookupByLibrary.simpleMessage("Inicial"),
    "buttonNavigationBarPetText" : MessageLookupByLibrary.simpleMessage("Pet"),
    "buttonNavigationBarProfileText" : MessageLookupByLibrary.simpleMessage("Perfil"),
    "buttonNavigationBarSettingText" : MessageLookupByLibrary.simpleMessage("Configuração"),
    "invalidDialog1Text" : MessageLookupByLibrary.simpleMessage("Ha campos vazios."),
    "invalidDialog2Text" : MessageLookupByLibrary.simpleMessage("Erro durante a criação do pet."),
    "invalidDialog3Text" : MessageLookupByLibrary.simpleMessage("Erro durante o armazenamento do endereço."),
    "invalidDialog4Text" : MessageLookupByLibrary.simpleMessage("Usuário inválido."),
    "invalidDialog5Text" : MessageLookupByLibrary.simpleMessage("As senhas inseridas não são iguais."),
    "invalidDialog6Text" : MessageLookupByLibrary.simpleMessage("O usuario ja existe.\nTente com um Nome diferente."),
    "loginEnterTextButton" : MessageLookupByLibrary.simpleMessage("Entrar"),
    "loginForgotPasswordText" : MessageLookupByLibrary.simpleMessage("Senha Esquecida"),
    "loginInvalidDialogText" : MessageLookupByLibrary.simpleMessage("Nome ou senha incorreto."),
    "loginRegisterText" : MessageLookupByLibrary.simpleMessage("Registrar"),
    "loginText" : MessageLookupByLibrary.simpleMessage("Login"),
    "nameText" : MessageLookupByLibrary.simpleMessage("Nome"),
    "passwordConfirmText" : MessageLookupByLibrary.simpleMessage("Confirmar Senha"),
    "passwordText" : MessageLookupByLibrary.simpleMessage("Senha"),
    "petGalleryText" : MessageLookupByLibrary.simpleMessage("Galeria"),
    "petPetNameText" : MessageLookupByLibrary.simpleMessage("Nome do Pet"),
    "petPetRegistrationText" : MessageLookupByLibrary.simpleMessage("Registro do Pet"),
    "rateText" : MessageLookupByLibrary.simpleMessage("Avaliação"),
    "userNameText" : MessageLookupByLibrary.simpleMessage("Nome do Usuário"),
    "userTitleText" : MessageLookupByLibrary.simpleMessage("Registro do Usuário")
  };
}
