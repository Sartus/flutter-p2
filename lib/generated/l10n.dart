// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Cancel`
  String get buttonCancelText {
    return Intl.message(
      'Cancel',
      name: 'buttonCancelText',
      desc: '',
      args: [],
    );
  }

  /// `Create`
  String get buttonCreateText {
    return Intl.message(
      'Create',
      name: 'buttonCreateText',
      desc: '',
      args: [],
    );
  }

  /// `There are empty fields.`
  String get invalidDialog1Text {
    return Intl.message(
      'There are empty fields.',
      name: 'invalidDialog1Text',
      desc: '',
      args: [],
    );
  }

  /// `Error during pet creation.`
  String get invalidDialog2Text {
    return Intl.message(
      'Error during pet creation.',
      name: 'invalidDialog2Text',
      desc: '',
      args: [],
    );
  }

  /// `Error while storing the address.`
  String get invalidDialog3Text {
    return Intl.message(
      'Error while storing the address.',
      name: 'invalidDialog3Text',
      desc: '',
      args: [],
    );
  }

  /// `Invalid user.`
  String get invalidDialog4Text {
    return Intl.message(
      'Invalid user.',
      name: 'invalidDialog4Text',
      desc: '',
      args: [],
    );
  }

  /// `The passwords entered are not the same.`
  String get invalidDialog5Text {
    return Intl.message(
      'The passwords entered are not the same.',
      name: 'invalidDialog5Text',
      desc: '',
      args: [],
    );
  }

  /// `The user already exists.\nTry with a different Name.`
  String get invalidDialog6Text {
    return Intl.message(
      'The user already exists.\nTry with a different Name.',
      name: 'invalidDialog6Text',
      desc: '',
      args: [],
    );
  }

  /// `Home`
  String get buttonNavigationBarHomeText {
    return Intl.message(
      'Home',
      name: 'buttonNavigationBarHomeText',
      desc: '',
      args: [],
    );
  }

  /// `Profile`
  String get buttonNavigationBarProfileText {
    return Intl.message(
      'Profile',
      name: 'buttonNavigationBarProfileText',
      desc: '',
      args: [],
    );
  }

  /// `Address`
  String get buttonNavigationBarAddressText {
    return Intl.message(
      'Address',
      name: 'buttonNavigationBarAddressText',
      desc: '',
      args: [],
    );
  }

  /// `Setting`
  String get buttonNavigationBarSettingText {
    return Intl.message(
      'Setting',
      name: 'buttonNavigationBarSettingText',
      desc: '',
      args: [],
    );
  }

  /// `Pet`
  String get buttonNavigationBarPetText {
    return Intl.message(
      'Pet',
      name: 'buttonNavigationBarPetText',
      desc: '',
      args: [],
    );
  }

  /// `User Name`
  String get userNameText {
    return Intl.message(
      'User Name',
      name: 'userNameText',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get passwordText {
    return Intl.message(
      'Password',
      name: 'passwordText',
      desc: '',
      args: [],
    );
  }

  /// `Confirm the Password`
  String get passwordConfirmText {
    return Intl.message(
      'Confirm the Password',
      name: 'passwordConfirmText',
      desc: '',
      args: [],
    );
  }

  /// `Name`
  String get nameText {
    return Intl.message(
      'Name',
      name: 'nameText',
      desc: '',
      args: [],
    );
  }

  /// `Address`
  String get addressText {
    return Intl.message(
      'Address',
      name: 'addressText',
      desc: '',
      args: [],
    );
  }

  /// `Rate`
  String get rateText {
    return Intl.message(
      'Rate',
      name: 'rateText',
      desc: '',
      args: [],
    );
  }

  /// `Login`
  String get loginText {
    return Intl.message(
      'Login',
      name: 'loginText',
      desc: '',
      args: [],
    );
  }

  /// `Forgot Password`
  String get loginForgotPasswordText {
    return Intl.message(
      'Forgot Password',
      name: 'loginForgotPasswordText',
      desc: '',
      args: [],
    );
  }

  /// `Register`
  String get loginRegisterText {
    return Intl.message(
      'Register',
      name: 'loginRegisterText',
      desc: '',
      args: [],
    );
  }

  /// `Enter`
  String get loginEnterTextButton {
    return Intl.message(
      'Enter',
      name: 'loginEnterTextButton',
      desc: '',
      args: [],
    );
  }

  /// `Name or password incorrect.`
  String get loginInvalidDialogText {
    return Intl.message(
      'Name or password incorrect.',
      name: 'loginInvalidDialogText',
      desc: '',
      args: [],
    );
  }

  /// `Pet Registration`
  String get petPetRegistrationText {
    return Intl.message(
      'Pet Registration',
      name: 'petPetRegistrationText',
      desc: '',
      args: [],
    );
  }

  /// `Pet Name`
  String get petPetNameText {
    return Intl.message(
      'Pet Name',
      name: 'petPetNameText',
      desc: '',
      args: [],
    );
  }

  /// `Gallery`
  String get petGalleryText {
    return Intl.message(
      'Gallery',
      name: 'petGalleryText',
      desc: '',
      args: [],
    );
  }

  /// `User's Address`
  String get addressTitleText {
    return Intl.message(
      'User\'s Address',
      name: 'addressTitleText',
      desc: '',
      args: [],
    );
  }

  /// `Number`
  String get addressNumberText {
    return Intl.message(
      'Number',
      name: 'addressNumberText',
      desc: '',
      args: [],
    );
  }

  /// `Complementary`
  String get addressComplementaryText {
    return Intl.message(
      'Complementary',
      name: 'addressComplementaryText',
      desc: '',
      args: [],
    );
  }

  /// `Neighborhood`
  String get addressNeighborhoodText {
    return Intl.message(
      'Neighborhood',
      name: 'addressNeighborhoodText',
      desc: '',
      args: [],
    );
  }

  /// `City`
  String get addressCityText {
    return Intl.message(
      'City',
      name: 'addressCityText',
      desc: '',
      args: [],
    );
  }

  /// `State`
  String get addressStateText {
    return Intl.message(
      'State',
      name: 'addressStateText',
      desc: '',
      args: [],
    );
  }

  /// `User Registration`
  String get userTitleText {
    return Intl.message(
      'User Registration',
      name: 'userTitleText',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'pt', countryCode: 'BR'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}