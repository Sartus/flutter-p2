

import 'package:flutter_test/flutter_test.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/controller/pet_owner_controller.dart';
import 'package:pet/db/server_conn_db.dart';
import 'package:pet/model/pet_owner_model.dart';

final PetOwnerController  ctr = PetOwnerController(type: EnumSideType.server);

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  tearDownAll(() async {
    // limpa o banco apos todos os testes.
    var db = ServerDatabase.instance;
    db.deleteDb();
  });
  group("ControllerPetOwner tests", () {
        test("\nSucesso: Insert de linha vazia na tabela, \n" + 
      "insert de linha com id=9876, update na linha com id=9876.\n" + 
      "Entidade: ControllerPetOwner\n" +
      "Condição de Teste: “retorno == id” " +
      "Resultado Esperado: “All tests passed” ",
      () async {
      int resultInsertVazio = await ctr.insert(PetOwnerModel());
      print(resultInsertVazio);
      expect(resultInsertVazio > 0, true);

      int resultInsertObj = await ctr.insert(PetOwnerModel(idPetOwner: 9876));
      expect(resultInsertObj == 9876, true);
      print(resultInsertObj);

      int resultUpdate = await ctr.update(PetOwnerModel(idPetOwner: 9876));
      expect(resultUpdate == 1, true);
    });
    test("\nSucesso: Busca pele linha que contem id=1,"+
      " busca por todas as colunas," +
      " busca por um numero limitado de colunas.\n" +
      "Entidade: ControllerPetOwner\n" +
      "Condição de Teste: “retorno != null” \n" +
      "Resultado Esperado: “All tests passed” \n",
      () async {
      PetOwnerModel resultgetById = await ctr.getById(1);
      expect(resultgetById != null, true);

      List<PetOwnerModel> resultgetAllRows = await ctr.getAllRows();
      expect(resultgetAllRows.isNotEmpty, true);

      List<PetOwnerModel> resultgetRows = await ctr.getRows();
      expect(resultgetRows.length > 0, true);
    });
    test("\nERRO : Busca por id inexistente, Insert id existente,"+
      " Update id inexistente.\n" + 
      "Entidade: ControllerPetOwner\n" +
      "Condição de Teste: “obj == null”\n" +
      "Resultado Esperado: “All tests passed”\n", 
    () async {
      PetOwnerModel resultgetByIdError = await ctr.getById(0);
      expect(resultgetByIdError == null, true);
      int resultInsertObj;
      
      resultInsertObj = await ctr.insert(PetOwnerModel(idPetOwner: 9876));
      
      expect(resultInsertObj == 0, true);

      int resultUpdate = await ctr.update(PetOwnerModel(idPetOwner: 48452));
      expect(resultUpdate == 0, true);
    });
    test("\nSucesso: Delete em ID(9876) existente \n" + 
      "Entidade: ControllerPetOwner\n" +
      "Condição de Teste: “retorno == 1” \n" +
      "Resultado Esperado: “All tests passed” \n",
      () async {
      int resultDelete = await ctr.delete(9876);
      expect(resultDelete == 1, true);
    });
    test("\nErro : Busca por id (9876) inexistente.\n" + 
      "Entidade: ControllerPetOwner\n" +
      "Condição de Teste: “obj == null”\n" +
      "Resultado Esperado: “All tests passed”\n", 
    () async {
      PetOwnerModel resultgetByIdError = await ctr.getById(9876);
      expect(resultgetByIdError == null, true);
    });
    test("\nErro: Delete em id(9876) inexistente \n" + 
      "Entidade: ControllerPetOwner \n" +
      "Condição de Teste: “retorno == 0” \n" +
      "Resultado Esperado: “All tests passed” \n",
      () async {
      int resultDelete = await ctr.delete(9876);
      expect(resultDelete == 0, true);
    });
  });
}