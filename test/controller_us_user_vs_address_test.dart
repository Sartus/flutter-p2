

import 'package:flutter_test/flutter_test.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/controller/us_user_vs_address_controller.dart';
import 'package:pet/db/server_conn_db.dart';
import 'package:pet/model/us_user_model.dart';
import 'package:pet/model/us_user_vs_address_model.dart';

final UserVsAddressController  ctr = UserVsAddressController(type: EnumSideType.server);

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  tearDownAll(() async {
    // limpa o banco apos todos os testes.
    var db = ServerDatabase.instance;
    db.deleteDb();
  });
  group("ControllerUserVsAddress tests", () {
    test("Sucesso: Busca pele linha que contem id=1, busca por todas as colunas," +
      " busca por um numero limitado de colunas.\n" +
      "Entidade: ControllerUserVsAddress\n" +
      "Condição de Teste: “retorno != null” \n" +
      "Resultado Esperado: “All tests passed” \n",
      () async {
      UserVsAddressModel resultgetById = await ctr.getById(1);
      expect(resultgetById != null, true);

      List<UserVsAddressModel> resultgetAllRows = await ctr.getAllRows();
      expect(resultgetAllRows.isNotEmpty, true);

      List<UserVsAddressModel> resultgetRows = await ctr.getRows();
      expect(resultgetRows.length == 10, true);
    });
    test("\Erro : Busca por id (9876) inexistente.\n" + 
      "Entidade: ControllerUserVsAddress\n" +
      "Condição de Teste: “obj == null”\n" +
      "Resultado Esperado: “All tests passed”\n", 
    () async {
      UserVsAddressModel resultgetByIdError = await ctr.getById(9876);
      expect(resultgetByIdError == null, true);
    });
    test("Sucesso: Insert de linha vazia na tabela, " + 
      "insert de linha com id=9876, update na linha com id=9876." + 
      "Entidade: ControllerUserVsAddress\n" +
      "Condição de Teste: “retorno == id” " +
      "Resultado Esperado: “All tests passed” ",
      () async {
      int resultInsertVazio = await ctr.insert(UserVsAddressModel());
      print(resultInsertVazio);
      expect(resultInsertVazio > 0, true);

      int resultInsertObj = await ctr.insert(UserVsAddressModel(idUserVsAddress: 9876));
      expect(resultInsertObj == 9876, true);
      print(resultInsertObj);

      int resultUpdate = await ctr.update(UserVsAddressModel(idUserVsAddress: 9876,
       number: "031"));
      expect(resultUpdate == 1, true);
    });
    test("\nERRO : Busca por id inexistente, Insert id existente, Update id inexistente.\n" + 
      "Entidade: ControllerUserVsAddress\n" +
      "Condição de Teste: “obj == null”\n" +
      "Resultado Esperado: “All tests passed”\n", 
    () async {
      UserVsAddressModel resultgetByIdError = await ctr.getById(0);
      expect(resultgetByIdError == null, true);
      int resultInsertObj;
      
      resultInsertObj = await ctr.insert(UserVsAddressModel(idUserVsAddress: 9876));
      
      expect(resultInsertObj == 0, true);

      int resultUpdate = await ctr.update(UserVsAddressModel(idUserVsAddress: 48452));
      expect(resultUpdate == 0, true);
    });
    test("Sucesso: Delete em ID(9876) existente" + 
      "Entidade: ControllerUserVsAddress\n" +
      "Condição de Teste: “retorno == 1” " +
      "Resultado Esperado: “All tests passed” ",
      () async {
      int resultDelete = await ctr.delete(9876);
      expect(resultDelete == 1, true);
    });
    test("Erro: Delete em ID(9876) inexistente" + 
      "Entidade: ControllerUserVsAddress\n" +
      "Condição de Teste: “retorno == 0” " +
      "Resultado Esperado: “All tests passed” ",
      () async {
      int resultDelete = await ctr.delete(9876);
      expect(resultDelete == 0, true);
    });
    test("Sucesso: Matchmaker com User de id(79)" + 
      "Entidade: ControllerUserVsAddress\n" +
      "Condição de Teste: “ result.first.values.first == 9 ” " +
      "Resultado Esperado: “All tests passed” ",
      () async {
      List<Map<String, dynamic>> result = await ctr.matchMaker(UserModel(idUser: 139));
      // print(result);
      expect(result.first.values.first == 9, true);
    });
  });
}