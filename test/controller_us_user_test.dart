

import 'package:flutter_test/flutter_test.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/controller/us_user_controller.dart';
import 'package:pet/db/server_conn_db.dart';
import 'package:pet/model/us_user_model.dart';

final UserController  ctr = UserController(type: EnumSideType.server);

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  tearDownAll(() async {
    // limpa o banco apos todos os testes.
    var db = ServerDatabase.instance;
    db.deleteDb();
  });
  group("ControllerUser tests", () {
    test("\nSucesso: Busca pele linha que contem id=1,"+
      " busca por todas as colunas," +
      " busca por um numero limitado de colunas.\n" +
      "Entidade: ControllerUser\n" +
      "Condição de Teste: “retorno != null” \n" +
      "Resultado Esperado: “All tests passed” \n",
      () async {
      UserModel resultgetById = await ctr.getById(1);
      expect(resultgetById != null, true);

      List<UserModel> resultgetAllRows = await ctr.getAllRows();
      expect(resultgetAllRows.isNotEmpty, true);

      List<UserModel> resultgetRows = await ctr.getRows();
      expect(resultgetRows.length == 10, true);
    });
    test("\nErro : Busca por id (9876) inexistente.\n" + 
      "Entidade: ControllerUser\n" +
      "Condição de Teste: “obj == null”\n" +
      "Resultado Esperado: “All tests passed”\n", 
    () async {
      UserModel resultgetByIdError = await ctr.getById(9876);
      expect(resultgetByIdError == null, true);
    });
    test("\nSucesso: Insert de linha vazia na tabela, \n" + 
      "insert de linha com id=9876, update na linha com id=9876.\n" + 
      "Entidade: ControllerUser\n" +
      "Condição de Teste: “retorno == id” " +
      "Resultado Esperado: “All tests passed” ",
      () async {
      int resultInsertVazio = await ctr.insert(UserModel());
      print(resultInsertVazio);
      expect(resultInsertVazio > 0, true);

      int resultInsertObj = await ctr.insert(UserModel(idUser: 9876));
      expect(resultInsertObj == 9876, true);
      print(resultInsertObj);

      int resultUpdate = await ctr.update(UserModel(idUser: 9876,
       relativePathNamePicture: "algo"));
      expect(resultUpdate == 1, true);
    });
    test("\nERRO : Busca por id inexistente, Insert id existente,"+
      " Update id inexistente.\n" + 
      "Entidade: ControllerUser\n" +
      "Condição de Teste: “obj == null”\n" +
      "Resultado Esperado: “All tests passed”\n", 
    () async {
      UserModel resultgetByIdError = await ctr.getById(0);
      expect(resultgetByIdError == null, true);
      int resultInsertObj;
      
      resultInsertObj = await ctr.insert(UserModel(idUser: 9876));
      
      expect(resultInsertObj == 0, true);

      int resultUpdate = await ctr.update(UserModel(idUser: 48452));
      expect(resultUpdate == 0, true);
    });
    test("\nSucesso: Delete em id(9876) existente \n" + 
      "Entidade: ControllerUser\n" +
      "Condição de Teste: “retorno == 1” \n" +
      "Resultado Esperado: “All tests passed” \n",
      () async {
      int resultDelete = await ctr.delete(9876);
      expect(resultDelete == 1, true);
    });
    test("\nErro: Delete em id(9876) inexistente \n" + 
      "Entidade: ControllerUser \n" +
      "Condição de Teste: “retorno == 0” \n" +
      "Resultado Esperado: “All tests passed” \n",
      () async {
      int resultDelete = await ctr.delete(9876);
      expect(resultDelete == 0, true);
    });
  });
}