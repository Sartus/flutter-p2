

import 'package:flutter_test/flutter_test.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/controller/pet_vs_pet_walker_controller.dart';
import 'package:pet/db/server_conn_db.dart';
// import 'package:pet/model/pet_vs_pet_walker_model.dart';

final PetVsPetWalkerController  ctr = PetVsPetWalkerController(type: EnumSideType.server);

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  tearDownAll(() async {
    // limpa o banco apos todos os testes.
    var db = ServerDatabase.instance;
    db.deleteDb();
  });
  test("Query all", () async{
    List<int> result = await ctr.getDistinctPetWalkerId();
    print(result.length);
    expect(result.length > 0, true);
  });
  // group("ControllerPetVsPetWalker tests", () {
  //   test("Sucesso: Insert de linha vazia na tabela, " + 
  //     "insert de linha com id=9876, update na linha com id=9876." + 
  //     "Entidade: ControllerPetVsPetWalker\n" +
  //     "Condição de Teste: “retorno == id” " +
  //     "Resultado Esperado: “All tests passed” ",
  //     () async {
  //     int resultInsertVazio = await ctr.insert(PetVsPetWalkerModel());
  //     expect(resultInsertVazio > 0, true);

  //     int resultInsertObj = await ctr.insert(PetVsPetWalkerModel(idPetVsPetWalker: 9876));
  //     expect(resultInsertObj == 9876, true);

  //     int resultUpdate = await ctr.update(PetVsPetWalkerModel(idPetVsPetWalker: 9876));
  //     expect(resultUpdate == 1, true);
  //   });
  //   test("Sucesso: Busca pele linha que contem id=1, busca por todas as colunas," +
  //     " busca por um numero limitado de colunas." +
  //     "Entidade: ControllerPetVsPetWalker\n" +
  //     "Condição de Teste: “retorno != null” " +
  //     "Resultado Esperado: “All tests passed” ",
  //     () async {
  //     PetVsPetWalkerModel resultgetById = await ctr.getById(1);
  //     expect(resultgetById != null, true);

  //     List<PetVsPetWalkerModel> resultgetAllRows = await ctr.getAllRows();
  //     expect(resultgetAllRows.isNotEmpty, true);

  //     List<PetVsPetWalkerModel> resultgetRows = await ctr.getRows();
  //     expect(resultgetRows.length > 0, true);
  //   });
  //   test("\nERRO : Busca por id inexistente, Insert id existente, Update id inexistente.\n" + 
  //     "Entidade: ControllerPetWalker\n" +
  //     "Condição de Teste: “obj == null”\n" +
  //     "Resultado Esperado: “All tests passed”\n", 
  //   () async {
  //     PetVsPetWalkerModel resultgetByIdError = await ctr.getById(0);
  //     expect(resultgetByIdError == null, true);
  //     int resultInsertObj;
      
  //     resultInsertObj = await ctr.insert(PetVsPetWalkerModel(idPetVsPetWalker: 9876));
      
  //     expect(resultInsertObj == 0, true);

  //     int resultUpdate = await ctr.update(PetVsPetWalkerModel(idPetVsPetWalker: 48452));
  //     expect(resultUpdate == 0, true);
  //   });
  //   test("Sucesso: Delete em ID(9876) existente" + 
  //     "Entidade: ControllerPetVsPetWalker\n" +
  //     "Condição de Teste: “retorno == 1” " +
  //     "Resultado Esperado: “All tests passed” ",
  //     () async {
  //     int resultDelete = await ctr.delete(9876);
  //     expect(resultDelete == 1, true);
  //   });
  //   test("\Erro : Busca por id (9876) inexistente.\n" + 
  //     "Entidade: ControllerPetVsPetWalker\n" +
  //     "Condição de Teste: “obj == null”\n" +
  //     "Resultado Esperado: “All tests passed”\n", 
  //   () async {
  //     PetVsPetWalkerModel resultgetByIdError = await ctr.getById(9876);
  //     expect(resultgetByIdError == null, true);
  //   });
  //   test("Erro: Delete em ID(9876) inexistente" + 
  //     "Entidade: ControllerPetVsPetWalker\n" +
  //     "Condição de Teste: “retorno == 0” " +
  //     "Resultado Esperado: “All tests passed” ",
  //     () async {
  //     int resultDelete = await ctr.delete(9876);
  //     expect(resultDelete == 0, true);
  //   });
  // });
}