

import 'package:flutter_test/flutter_test.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/controller/pet_special_care_tp_controller.dart';
import 'package:pet/db/server_conn_db.dart';
import 'package:pet/model/pet_special_care_tp_model.dart';

final PetSpecialCareTpController  ctr = PetSpecialCareTpController(type: EnumSideType.server);

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  tearDownAll(() async {
    // limpa o banco apos todos os testes.
    var db = ServerDatabase.instance;
    db.deleteDb();
  });
  group("ControllerPetSpecialCareTp tests", () {
        test("\nSucesso: Insert de linha vazia na tabela, \n" + 
      "insert de linha com id=9876, update na linha com id=9876.\n" + 
      "Entidade: ControllerPetSpecialCareTp\n" +
      "Condição de Teste: “retorno == id” " +
      "Resultado Esperado: “All tests passed” ",
      () async {
      int resultInsertVazio = await ctr.insert(PetSpecialCareTpModel());
      print(resultInsertVazio);
      expect(resultInsertVazio > 0, true);

      int resultInsertObj = await ctr.insert(PetSpecialCareTpModel(idPetSpecialCareTp: 9876));
      expect(resultInsertObj == 9876, true);
      print(resultInsertObj);

      int resultUpdate = await ctr.update(PetSpecialCareTpModel(idPetSpecialCareTp: 9876));
      expect(resultUpdate == 1, true);
    });
    test("\nSucesso: Busca pele linha que contem id=1,"+
      " busca por todas as colunas," +
      " busca por um numero limitado de colunas.\n" +
      "Entidade: ControllerPetSpecialCareTp\n" +
      "Condição de Teste: “retorno != null” \n" +
      "Resultado Esperado: “All tests passed” \n",
      () async {
      PetSpecialCareTpModel resultgetById = await ctr.getById(1);
      expect(resultgetById != null, true);

      List<PetSpecialCareTpModel> resultgetAllRows = await ctr.getAllRows();
      expect(resultgetAllRows.isNotEmpty, true);

      List<PetSpecialCareTpModel> resultgetRows = await ctr.getRows();
      expect(resultgetRows.length > 0, true);
    });
    test("\nERRO : Busca por id inexistente, Insert id existente,"+
      " Update id inexistente.\n" + 
      "Entidade: ControllerPetSpecialCareTp\n" +
      "Condição de Teste: “obj == null”\n" +
      "Resultado Esperado: “All tests passed”\n", 
    () async {
      PetSpecialCareTpModel resultgetByIdError = await ctr.getById(0);
      expect(resultgetByIdError == null, true);
      int resultInsertObj;
      
      resultInsertObj = await ctr.insert(PetSpecialCareTpModel(idPetSpecialCareTp: 9876));
      
      expect(resultInsertObj == 0, true);

      int resultUpdate = await ctr.update(PetSpecialCareTpModel(idPetSpecialCareTp: 48452));
      expect(resultUpdate == 0, true);
    });
    test("\nSucesso: Delete em ID(9876) existente \n" + 
      "Entidade: ControllerPetSpecialCareTp\n" +
      "Condição de Teste: “retorno == 1” \n" +
      "Resultado Esperado: “All tests passed” \n",
      () async {
      int resultDelete = await ctr.delete(9876);
      expect(resultDelete == 1, true);
    });
    test("\nErro : Busca por id (9876) inexistente.\n" + 
      "Entidade: ControllerPetSpecialCareTp\n" +
      "Condição de Teste: “obj == null”\n" +
      "Resultado Esperado: “All tests passed”\n", 
    () async {
      PetSpecialCareTpModel resultgetByIdError = await ctr.getById(9876);
      expect(resultgetByIdError == null, true);
    });
    test("\nErro: Delete em id(9876) inexistente \n" + 
      "Entidade: ControllerPetSpecialCareTp \n" +
      "Condição de Teste: “retorno == 0” \n" +
      "Resultado Esperado: “All tests passed” \n",
      () async {
      int resultDelete = await ctr.delete(9876);
      expect(resultDelete == 0, true);
    });
  });
}