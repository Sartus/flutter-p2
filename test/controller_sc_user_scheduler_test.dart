

import 'package:flutter_test/flutter_test.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/controller/sc_user_scheduler_controller.dart';
import 'package:pet/db/server_conn_db.dart';
import 'package:pet/model/sc_user_scheduler_model.dart';

final UserSchedulerController  ctr = UserSchedulerController(type: EnumSideType.server);

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  tearDownAll(() async {
    // limpa o banco apos todos os testes.
    var db = ServerDatabase.instance;
    db.deleteDb();
  });
  group("ControllerUserScheduler tests", () {
    test("Sucesso: Insert de linha vazia na tabela, " + 
      "insert de linha com id=9876, update na linha com id=9876." + 
      "Entidade: ControllerUserScheduler\n" +
      "Condição de Teste: “retorno == id” " +
      "Resultado Esperado: “All tests passed” ",
      () async {
      int resultInsertVazio = await ctr.insert(UserSchedulerModel());
      print(resultInsertVazio);
      expect(resultInsertVazio > 0, true);

      int resultInsertObj = await ctr.insert(UserSchedulerModel(idUserSchedule: 9876));
      expect(resultInsertObj == 9876, true);

      int resultUpdate = await ctr.update(UserSchedulerModel(idUserSchedule: 9876,
       title: "TESTE", beginTs: DateTime.now(), endTs: DateTime.now()));
      print(await ctr.getById(9876));
      expect(resultUpdate == 1, true);
    });
    test("\nERRO : Busca por id inexistente, Insert id existente, Update id inexistente.\n" + 
      "Entidade: ControllerUserScheduler\n" +
      "Condição de Teste: “obj == null”\n" +
      "Resultado Esperado: “All tests passed”\n", 
    () async {
      UserSchedulerModel resultgetByIdError = await ctr.getById(0);
      expect(resultgetByIdError == null, true);
      int resultInsertObj;
      
      resultInsertObj = await ctr.insert(UserSchedulerModel(idUserSchedule: 9876));
      
      expect(resultInsertObj == 0, true);

      int resultUpdate = await ctr.update(UserSchedulerModel(idUserSchedule: 48452));
      expect(resultUpdate == 0, true);
    });
    test("Sucesso: Busca pele linha que contem id=1, busca por todas as colunas," +
      " busca por um numero limitado de colunas.\n" +
      "Entidade: ControllerUserScheduler\n" +
      "Condição de Teste: “retorno != null” \n" +
      "Resultado Esperado: “All tests passed” \n",
      () async {
      UserSchedulerModel resultgetById = await ctr.getById(1);
      expect(resultgetById != null, true);

      List<UserSchedulerModel> resultgetAllRows = await ctr.getAllRows();
      expect(resultgetAllRows.isNotEmpty, true);

      List<UserSchedulerModel> resultgetRows = await ctr.getRows();
      expect(resultgetRows.length > 0, true);
    });
    test("Sucesso: Delete em ID(9876) existente" + 
      "Entidade: ControllerUserScheduler\n" +
      "Condição de Teste: “retorno == 1” " +
      "Resultado Esperado: “All tests passed” ",
      () async {
      int resultDelete = await ctr.delete(9876);
      expect(resultDelete == 1, true);
    });
    test("\Erro : Busca por id (9876) inexistente.\n" + 
      "Entidade: ControllerUserScheduler\n" +
      "Condição de Teste: “obj == null”\n" +
      "Resultado Esperado: “All tests passed”\n", 
    () async {
      UserSchedulerModel resultgetByIdError = await ctr.getById(9876);
      expect(resultgetByIdError == null, true);
    });
  });
}